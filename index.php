<?php $version = 2; ?>
<!DOCTYPE HTML>
<html>
<head>
    <title>conceptninja.co.za - WebGL at Sea</title>
    <meta name="description" content="WebGL Star Wars scene built in three.js" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicons/favicon-16x16.png">
    <link rel="manifest" href="images/favicons/site.webmanifest">
    <link rel="mask-icon" href="images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/main.css?v=<?php echo $version; ?>" />
    <script src="js/3rdParty/lazy.min.js?v=<?php echo $version; ?>" async=""></script>
</head>
<body>
<div class="body">
    <canvas id="threejs"></canvas>
    <div id='soundBars' class="">
            <!--<div class="play-button"></div>-->
        <a class="play-btn" href="#"></a>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
        <div class='bar hidebar'></div>
    </div>
    <div class="section fp-auto-height" data-section-name="about">
        <div class="webglHolderSingle" id="test">
            <div class="webglInnerHolder">
                <div class="container0" id="container0"></div>
            </div>
        </div>
    </div>
</div>
<div id="blocker">
    <div id="instructions">
        <span style="font-size:24px">Click for action</span><br/>&nbsp;<br />
    </div>
</div>
<div>
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div id="loader_text">Loading...</div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
</div>
<!-- jQuery -->
<script src="js/3rdParty/jquery-3.5.1.min.js?v=<?php echo $version; ?>"></script>

<script src="js/common/rStats.js?v=<?php echo $version; ?>"></script>
<script src="js/common/rStats.extras.js?v=<?php echo $version; ?>"></script>

<!-- Main program script -->
<script src="js/common/vars.js?v=<?php echo $version; ?>"></script>

<script type="module" src="js/main.js?v=<?php echo $version; ?>"></script>
<!--<script type="module" src="js/bundle.min.js?v=<?php /*echo $version; */?>"></script>-->
<script type="module" src="js/extra.js?v=<?php echo $version; ?>"></script>
</body>
</html>