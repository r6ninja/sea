<?php
include_once('globals.php');
if (!$CURRENT_USER)
    die('No permission to access this page.');

require_once($_SERVER['DOCUMENT_ROOT'].'/account/company_functions.php');

// Set state to false by default
$state = false;

$requestData = $_REQUEST;
array_walk_recursive($requestData, function (&$value) {
    $value = htmlentities($value, ENT_QUOTES);
});

// Check what to update
switch($_REQUEST['updateThis']) {
    case 'account_setting' : $state = $Settings->accountSettings($requestData['data']); break;
    case 'account_profile' : $state = $Settings->accountProfile($requestData['data']); break;
    case 'remove_image' : $state = $Settings->removeImage($requestData['data']); break;
    case 'star_candidate' : $state = $Settings->starCandidate($requestData['data'], $CURRENT_USER); break;
    case 'contact_status' : $state = $Settings->contactStatus($requestData['data'], $CURRENT_USER); break;
    case 'message_candidate' : $state = $Settings->messageCandidate($requestData['data'], $CURRENT_USER); break;
    case 'edit_industries' : $state = $Settings->editIndustries($requestData['data'], $CURRENT_USER); break;
    case 'edit_skills' : $state = $Settings->editJobSkills($requestData['data'], $CURRENT_USER); break;
    case 'alert_settings' : $state = $Settings->alertSettings($requestData['data']); break;
    case 'report_candidate' : $state = $Settings->reportCandidate($requestData['data'], $CURRENT_USER); break;
    case 'dismiss_match' : $state = $Settings->dismissMatch($requestData['data'], $CURRENT_USER); break;
    default : break;
}

echo ($state) ? '{"status":"success", "data": "'.$state.'"}' : '{"status":"failed"}';
?>
