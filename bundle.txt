# Install rollup using: "npm install --global rollup"
rollup main.js --format iife --file bundle.js

then:

# Install uglifyjs using: "npm install uglify-js -g"
uglifyjs bundle.js -c -o bundle.min.js