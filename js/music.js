/**
 * Setup the lighting for the scene
 */
import * as THREE from './3rdParty/three.module.js';
import {TWEEN} from './3rdParty/tween.module.min.js';

THREE.Audio.prototype.playAgain = function(delay) {

    if ( this.hasPlaybackControl === false ) {
        console.warn( 'THREE.Audio: this Audio has no playback control.' );
        return;
    }

    this._startedAt = this.context.currentTime + delay;

    const source = this.context.createBufferSource();
    source.buffer = this.buffer;
    source.loop = this.loop;
    source.loopStart = this.loopStart;
    source.loopEnd = this.loopEnd;
    source.onended = this.onEnded.bind( this );
    source.start( 0, this._progress + this.offset, this.duration );

    this.isPlaying = true;

    this.source = source;

    this.setDetune( this.detune );
    this.setPlaybackRate( this.playbackRate );

    return this.connect();
}

export const music = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {

                let that = this;
                this.time = 0;
                this.volume = {x:1};
                this.initiated = false;

                this.listener = new THREE.AudioListener();
                this.actionedPlay = false;
                this.actionedFade = true;
                //this.currentScene.camera.add(listener);

                this.musicTrack = new THREE.Audio(this.listener);
                this.tieCannon = new THREE.Audio(this.listener);
                this.xwingCannon = new THREE.Audio(this.listener);
                this.blaster = new THREE.Audio(this.listener);
                this.blaster_rebel = new THREE.Audio(this.listener);
                const audioLoader = new THREE.AudioLoader();
                audioLoader.load( './sounds/theme.mp3', function( buffer ) {
                    that.musicTrack.setBuffer( buffer );
                    that.musicTrack.setLoop( true );
                    that.musicTrack.setVolume( 1.0 );
                });

                this.events();

                LOADED++;
            }

            update(e) {

                if(this.tweening) {
                    TWEEN.update(e);
                }
            }

            events() {
                let that = this;
                window.addEventListener('unlock', function() {
                    that.fadeIn();
                    MUSIC = true;
                    $('#soundBars .bar').each(function () {
                        $(this).toggleClass('playing');
                        $(this).toggleClass('hidebar');
                    });
                    $('#soundBars').toggleClass('playing');
                    $('.play-btn').toggleClass('playing');
                });
                window.addEventListener("pauseMusic", function(event) {
                    that.fadeOut();
                }, false);
                window.addEventListener("playMusic", function(event) {
                    that.fadeIn();
                }, false);
            }

            fadeOut() {
                let that = this;

                if(typeof this.fadeInTween !== 'undefined')
                    this.fadeInTween.stop();

                //this.time = 0;
                this.tweening = true;
                this.volume = {x:2};
                this.fadeOutTween = new TWEEN.Tween(this.volume).to({
                    x: 0
                }, 500).onUpdate(function(fade) {
                    that.musicTrack.setVolume(fade.x);
                }).onComplete(function() {
                    that.musicTrack.pause();
                    that.tweening = false;
                }).start();
            }

            stop() {
                this.musicTrack.pause();
            }

            play() {
                this.musicTrack.play();
            }

            fadeIn() {
                let that = this;

                if(typeof this.fadeOutTween !== 'undefined')
                    this.fadeOutTween.stop();

                //this.time = 0;
                this.tweening = true;
                this.musicTrack.play();
                this.musicTrack.setVolume(0);
                this.volume = {x:0};
                this.fadeInTween = new TWEEN.Tween(this.volume).to({
                    x: 2
                }, 1000).onUpdate(function(fade) {
                    that.musicTrack.setVolume(fade.x);
                }).onComplete(function() {
                    that.tweening = false;
                }).start();
            }
        }
    }
})();