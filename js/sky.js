/**
 * Setup the skybox for the scene
 */
import * as THREE from './3rdParty/three.module.js';
import { Sky } from './3rdParty/Sky.js';
import {GUI} from './3rdParty/dat.gui.module.js';
import {LinearSpline} from './LinearSpline.js';

export const sky = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {

                this.showGUI = false;
                this.makeProgress = false;
                this.clock = new THREE.Clock();
                this.tmp = new THREE.Vector3();
                this.positionVectorTemp = new THREE.Vector3(2, 180, 0.5);
                this.positionVectorMid = new THREE.Vector3(90, 0, 0.116);
                this.positionVectorEnd = new THREE.Vector3(-3, 0, 0.5);
                this.positionVectorStart = new THREE.Vector3(-20, 220, 0.5);

                this.createSplines();

                this.duration = 80000;
                this.timeDilation = 14000;

                this.playScene = false;
                this.rising = true;
                this.effectController = {
                    turbidity: 10,
                    rayleigh: 3,
                    mieCoefficient: 0.005,
                    mieDirectionalG: 0.7,
                    elevation: 2,
                    azimuth: 180,
                    exposure: this.currentScene.renderer.toneMappingExposure
                };
                this.customControls = {
                    progress:0.19,
                    autoProgress:false
                };

                // Add Sky
                this.sky = new Sky();
                this.sky.scale.setScalar( 450000 );
                this.currentScene.scene.add(this.sky);
                this.uniforms = this.sky.material.uniforms;

                this.sun = new THREE.Vector3();

                if(this.showGUI)
                    this.GUI();
                else
                    this.guiChanged(this);

                /*if(BETTER_FOG || BETTER_FOG2) {
                    sky.material.onBeforeCompile = ModifyShader_;
                }*/

                this.events();

                LOADED++;
            }

            createSplines() {
                this.RayleighSpline = new LinearSpline((t, a, b) => {
                    return a + t * (b - a);
                });
                this.RayleighSpline.AddPoint(0.0, 4.0);
                this.RayleighSpline.AddPoint(0.2, 3.07);
                this.RayleighSpline.AddPoint(0.25, 0.558);
                this.RayleighSpline.AddPoint(0.263, 0.313);
                this.RayleighSpline.AddPoint(0.32, 0.214);
                this.RayleighSpline.AddPoint(1.0, 0.116);

                this.MieCofSpline = new LinearSpline((t, a, b) => {
                    return a + t * (b - a);
                });
                this.MieCofSpline.AddPoint(0.0, 0.005);
                this.MieCofSpline.AddPoint(0.2, 0.005);
                this.MieCofSpline.AddPoint(0.242, 0.0005);
                this.MieCofSpline.AddPoint(1.0, 0.00001);

                this.MieDirectionalG = new LinearSpline((t, a, b) => {
                    return a + t * (b - a);
                });
                this.MieDirectionalG.AddPoint(0.0, 0.7);
                this.MieDirectionalG.AddPoint(0.2, 0.668);
                this.MieDirectionalG.AddPoint(0.242, 0.201);
                this.MieDirectionalG.AddPoint(1.0, 0.336);
            }

            GUI() {
                let that = this;
                const gui = new GUI();

                gui.add( this.effectController, 'turbidity', 0.0, 20.0, 0.1 ).listen().onChange( function () { that.guiChanged(that); });
                gui.add( this.effectController, 'rayleigh', 0.0, 4, 0.001 ).listen().onChange( function () { that.guiChanged(that); } );
                gui.add( this.effectController, 'mieCoefficient', 0.0, 0.1, 0.001 ).listen().onChange( function () { that.guiChanged(that); } );
                gui.add( this.effectController, 'mieDirectionalG', 0.0, 1, 0.001 ).listen().onChange( function () { that.guiChanged(that); } );
                gui.add( this.effectController, 'elevation', -3, 90, 0.1 ).listen().onChange( function () { that.guiChanged(that); } );
                gui.add( this.effectController, 'azimuth', - 180, 180, 0.1 ).listen().onChange( function () { that.guiChanged(that); } );
                gui.add( this.effectController, 'exposure', 0, 1, 0.0001 ).listen().onChange( function () { that.guiChanged(that); } );
                gui.add( this.customControls, 'progress', 0.01, 0.99, 0.01 ).listen();
                gui.add( this.customControls, 'autoProgress').listen().onChange( function(v) {
                    if(v) {
                        that.timeDilation = parseInt((that.duration*that.customControls.progress).toFixed(0));
                        that.startTime = undefined;
                    }
                });

                this.guiChanged(that);
            }

            updateGUI(vec) {
                this.effectController.rayleigh = this.uniforms[ 'rayleigh' ].value;
                this.effectController.mieCoefficient = this.uniforms[ 'mieCoefficient' ].value;
                this.effectController.mieDirectionalG = this.uniforms[ 'mieDirectionalG' ].value;
                this.effectController.elevation = vec.x;
                this.effectController.azimuth = vec.y;
            }

            guiChanged(that) {
                this.uniforms[ 'turbidity' ].value = that.effectController.turbidity;
                this.uniforms[ 'rayleigh' ].value = that.effectController.rayleigh;
                this.uniforms[ 'mieCoefficient' ].value = that.effectController.mieCoefficient;
                this.uniforms[ 'mieDirectionalG' ].value = that.effectController.mieDirectionalG;

                const phi = THREE.MathUtils.degToRad( 90 - that.effectController.elevation );
                const theta = THREE.MathUtils.degToRad( that.effectController.azimuth );

                that.sun.setFromSphericalCoords( 1, phi, theta );

                this.uniforms[ 'sunPosition' ].value.copy( that.sun );

                this.currentScene.renderer.toneMappingExposure = that.effectController.exposure;
                this.currentScene.renderer.render( that.currentScene.scene, that.currentScene.camera );
            }

            events() {
                let that= this;
                window.addEventListener('unlock', function() {
                    that.playScene = true;
                });

                window.addEventListener('keydown', function(e) {
                    if(e.code === 'BracketRight' && !that.customControls.autoProgress) {
                        that.customControls.progress += 0.001;
                        that.timeDilation = parseInt((that.duration*that.customControls.progress).toFixed(0));
                        that.startTime = undefined;
                        that.makeProgress = true;
                    }

                    if(e.code === 'BracketLeft' && !that.customControls.autoProgress) {
                        that.customControls.progress -= 0.001;
                        that.timeDilation = parseInt((that.duration*that.customControls.progress).toFixed(0));
                        that.startTime = undefined;
                        that.makeProgress = true;
                    }
                });

                window.addEventListener('keyup', function(e) {
                    if(e.code === 'Space') {
                        that.playScene = !that.playScene;
                        that.timeDilation = parseInt((that.duration*that.customControls.progress).toFixed(0));
                        that.startTime = undefined;
                    }

                    that.makeProgress = false;
                });
            }

            sineBetween(min, max, t) {
                return ((max - min) * Math.sin(t) + max + min) / 2;
            }

            update() {
                if(this.playScene) {
                    if(typeof this.startTime === 'undefined') { this.startTime = Date.now()-this.timeDilation; }

                    let rayleigh,mieCof,mieDir;

                    if(this.rising && this.positionVectorTemp.x === this.positionVectorMid.x) {
                        this.rising = false;
                        SUNSTATUS = false;
                        this.startTime = Date.now();
                    } else if(!this.rising && this.positionVectorTemp.x === this.positionVectorEnd.x) {
                        this.rising = true;
                        SUNSTATUS = true;
                        this.startTime = Date.now();
                        this.positionVectorStart = new THREE.Vector3(-20, 220, 0.5);
                    }

                    const progress = (this.customControls.autoProgress) ? Math.min((Date.now() - this.startTime) / this.duration, 1) : this.customControls.progress;
                    this.customControls.progress = progress;
                    WAVEPROGRESS = (this.customControls.autoProgress) ? progress : Math.min((Date.now() - this.startTime) / this.duration, 1);

                    if(this.makeProgress || this.customControls.autoProgress) {

                        SUNPROGRESS = (this.customControls.autoProgress) ? progress : Math.min((Date.now() - this.startTime) / this.duration, 1);

                        if (this.rising) {
                            this.tmp.copy(this.positionVectorMid).sub(this.positionVectorStart).multiplyScalar(progress);
                            this.positionVectorTemp.copy(this.positionVectorStart).add(this.tmp);
                            rayleigh = this.RayleighSpline.Get(progress);
                            mieCof = this.MieCofSpline.Get(progress);
                            mieDir = this.MieDirectionalG.Get(progress);
                        } else {
                            this.tmp.copy(this.positionVectorEnd).sub(this.positionVectorMid).multiplyScalar(progress);
                            this.positionVectorTemp.copy(this.positionVectorMid).add(this.tmp);
                            rayleigh = this.RayleighSpline.Get(1 - progress);
                            mieCof = this.MieCofSpline.Get(1 - progress);
                            mieDir = this.MieDirectionalG.Get(1 - progress);
                        }

                        this.uniforms['rayleigh'].value = rayleigh;
                        this.uniforms['mieCoefficient'].value = mieCof;
                        this.uniforms['mieDirectionalG'].value = mieDir;

                        const phi = THREE.MathUtils.degToRad(90 - this.positionVectorTemp.x);
                        //const theta = THREE.MathUtils.degToRad(this.positionVectorTemp.y);
                        const theta = THREE.MathUtils.degToRad((this.rising) ? 180 : 0);

                        this.sun.setFromSphericalCoords(1, phi, theta);
                        SUNPOSITION = this.sun;

                        this.uniforms['sunPosition'].value.copy(this.sun);

                        if (this.showGUI)
                            this.updateGUI(this.positionVectorTemp);

                    }
                }
            }
        }
    }
})();