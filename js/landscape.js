/**
 * Setup the lighting for the scene
 */
import * as THREE from './3rdParty/three.module.js';
const TextureLoader = new THREE.TextureLoader();
export const landscape = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                this.loadLandscape();
                LOADED++;
            }

            loadTexture() {
            }

            loadLandscape() {

                const that = this;
                const mountainTexture = new THREE.Texture();
                mountainTexture.generateMipmaps = false;
                mountainTexture.magFilter = THREE.LinearFilter;
                mountainTexture.minFilter = THREE.LinearFilter;
                TextureLoader.load( 'images/mountain.png', function ( image ) {
                    mountainTexture.image = image;
                    mountainTexture.needsUpdate = true;

                    const mountainsMaterial = new THREE.MeshLambertMaterial( {
                        map: mountainTexture.image,
                        transparent: true,
                        side: THREE.BackSide,
                        depthWrite: true
                    });

                    that.addMountain(2500, mountainsMaterial, 100, 555, 1.25, 1200);
                });

                const mountainTexture2 = new THREE.Texture();
                mountainTexture2.generateMipmaps = false;
                mountainTexture2.magFilter = THREE.LinearFilter;
                mountainTexture2.minFilter = THREE.LinearFilter;
                TextureLoader.load( 'images/mountains3.png', function ( image ) {
                    mountainTexture2.image = image;
                    mountainTexture2.needsUpdate = true;

                    const mountainsMaterial2 = new THREE.MeshLambertMaterial( {
                        map: mountainTexture2.image,
                        transparent: true,
                        side: THREE.BackSide,
                        depthWrite: true
                    });

                    that.addMountain(3000, mountainsMaterial2, 190, 180, 1.25, 1200);
                });

                // Add a black cylinder to hide the skybox under the water
                const cylinder = new THREE.Mesh(
                    new THREE.CylinderGeometry( 1500, 1500, 1500, 32, 1, true, 0,  3),
                    new THREE.MeshBasicMaterial( { color: new THREE.Color( 1, 1, 1 ), side: THREE.BackSide } )
                );
                cylinder.position.y = -800;
                cylinder.name = 'mountainCylinder';
                //this.currentScene.scene.add( cylinder );
            }

            addMountain(size, mountainsMaterial, degrees, posY, theta, height) {

                const mountains = new THREE.Mesh(
                    //new THREE.CylinderGeometry( size, size, 600, 32, 1, true, 0, 3 ),
                    new THREE.CylinderGeometry( size, size, height, 32, 1, true, 0, theta ),
                    mountainsMaterial
                );
                mountains.position.y = posY;
                mountains.rotation.y = THREE.Math.degToRad(degrees);
                mountains.name = 'mountain'+size;
                this.currentScene.scene.add( mountains );

            }
        }
    }
})();