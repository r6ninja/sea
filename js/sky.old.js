/**
 * Setup the skybox for the scene
 */
import * as THREE from './3rdParty/three.module.js';
const TextureLoader = new THREE.TextureLoader();
export const sky2 = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                var that = this;

                let sbName = "outdoor";
                //let fPath = "images/sky/eq/" + sbName + "/";
                let fPath = "images/sky/eq/" + sbName + "/t/";

                let skyGeo = new THREE.SphereBufferGeometry(sceneSize, 25, 25);
                let loader  = new THREE.TextureLoader();
                //let texture = loader.load(fPath+'tar3.jpg');
                //let texture = loader.load(fPath+'pebbles.jpg');
                //let texture = loader.load(fPath+'winter-on-road.jpg');
                let texture = loader.load(fPath+'desert.jpg');
                let material = new THREE.MeshBasicMaterial({
                    map: texture,
                });
                let sky = new THREE.Mesh(skyGeo, material);
                sky.material.side = THREE.BackSide;
                sky.name = 'skySphere';
                //sky.position.set(0, 40, 0);
                sky.position.set(0, 0, 0);
                sky.rotation.set(0, -0.970, 0);
                if(BETTER_FOG || BETTER_FOG2) {
                    sky.material.onBeforeCompile = ModifyShader_;
                }

                this.currentScene.scene.add(sky);

                LOADED++;
            }
        }
    }
})();