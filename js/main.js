import {WEBGL} from './3rdParty/WebGL.js';
import * as THREE from './3rdParty/three.module.js';
import {scene} from './scene.js';
import {instructions} from './instructions.js';
import {events} from './events.js';
import {orbitControls} from './orbitControls.js';
import {light} from './light.js';
import {sea} from './sea2.js';
import {sky} from './sky.js';
import {landscape} from './landscape.js';
//import {sky2} from './sky.old.js';
//import {turbine} from './turbine3.js';
import {ship} from './yacht.js';
import {music} from './music.js';
import {postShaders} from './postShaders_new.js';
import {BlendFunction, KernelSize} from "./3rdParty/postprocessing.esm.js";
import {stats} from './stats.js';

if(postShaders+light+instructions+sky+landscape+sea+ship+events+music+orbitControls+stats) console.log('All inc.');

if (WEBGL.isWebGLAvailable()) {

    var sceneContents = [];
    sceneContents[0] = [
        {'postShaders':[]},
        {'instructions':[]},
        {'events':[]},
        //{'turbine':[]},
        {'ship':[]},
        {'light':[]},
        {'sea':[]},
        {'sky':[]},
        {'landscape':[]},
        //{'music':[]},
        {'stats':[]}
    ];

    if(!FLUID_CAMERA)
        sceneContents[0].push({'orbitControls':[]});

    var scenesArray=[];

    let can = document.getElementById('threejs');
    var renderer = new THREE.WebGLRenderer({
        canvas: can,
        antialias: !POST_PROCESSING,
        alpha: false,
        powerPreference: "high-performance",
        stencil: !POST_PROCESSING,
        depth: !POST_PROCESSING,
        useWebGL2IfAvailable: true
    });
    renderer.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    renderer.setPixelRatio(WINDOW_PIXEL_RATIO);
    renderer.autoClear = false;
    //renderer.gammaOutput = true;
    //renderer.gammaFactor = 2.2;
    //renderer.outputEncoding = THREE.sRGBEncoding;
    //renderer.outputEncoding = THREE.LinearEncoding;
    //renderer.toneMapping = THREE.ReinhardToneMapping;
    renderer.physicallyCorrectLights = false;
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    // Setup the scenes and start animating them
    scenesArray[0] = new scene.Main(sceneSize*6, 'container0', renderer, {
        gui: false,
        bloom: {on:false, settings:{height:360, luminanceSmoothing:0.4, luminanceThreshold:0.0, resolutionScale:1.0, intensity:1.0, kernelSize:KernelSize.MEDIUM, blendFunction:BlendFunction.SCREEN}},
    //bloomSelect: {on:true, settings:{blendFunction: BlendFunction.COLOR_DODGE, kernelSize: KernelSize.LARGE, intensity: 5.2, luminanceThreshold: 0.1, luminanceSmoothing: 0.2, height: 720}},
    bloomSelect: {on:false, settings:{blendFunction: BlendFunction.COLOR_DODGE, kernelSize: KernelSize.SMALL, intensity: 5.2, luminanceThreshold: 0.1, luminanceSmoothing: 0.2, height: 720, blurScale:0.7}},
        scanlines: {on:false, settings:{blendFunction : BlendFunction.OVERLAY}},
    customNoise:{on:SELECT_PROCESSING, settings:{blendFunction : BlendFunction.AVERAGE, premultiply: true, opacity: 0.75, fullScreen: true, fadeBottom: false, fadeTop:false, emptyFadeTop:false, emptyFadeBottom:true, offSet: true}},
        haze:{on:false, settings:{blendFunction : BlendFunction.ALPHA, premultiply: true}},
        toneMapping:{on:false},
    //bokeh:{on:true, settings:{blendFunction: BlendFunction.NORMAL, focusDistance: 0.159, focalLength: 0.1258, bokehScale: 3.33}},
    //bokeh:{on:true, settings:{blendFunction: BlendFunction.NORMAL, focusDistance: 0.159, focalLength: 0.1148, bokehScale: 3.33}}, //==============================OPTION 1
    bokeh:{on:SELECT_PROCESSING, settings:{blendFunction: BlendFunction.NORMAL, focusDistance: 0.287, focalLength: 0.3977, bokehScale: 1.755}},
    vignette:{on:SELECT_PROCESSING}
    }, false, false);

    for(let c=0;c<scenesArray.length;c++) {
        for (let t = 0; t < sceneContents[c].length; t++) {
            let constructorName = Object.keys(sceneContents[c][t])[0];
            sceneContents[c][t][constructorName][0] = eval('new ' + constructorName + '.Main(scenesArray[c])');
        }
    }

    window.scene  = scenesArray[0].scene;
    window.camera = scenesArray[0].camera;
    window.THREE  = THREE;

    if(STATS) {
        var glS = new glStats();
        var tS = new threeStats(renderer);

        var rS = new rStats({
            CSSPath: 'css/',
            values: {
                frame: { caption: 'Total frame time (ms)', over: 16 },
                fps: { caption: 'Framerate (FPS)', below: 30 },
                calls: { caption: 'Calls (three.js)', over: 3000 },
                raf: { caption: 'Time since last rAF (ms)' },
                rstats: { caption: 'rStats update (ms)' }
            },
            groups: [
                { caption: 'Framerate', values: [ 'fps', 'raf' ] },
                { caption: 'Frame Budget', values: [ 'frame', 'texture', 'setup', 'render' ] }
            ],
            fractions: [
                { base: 'frame', steps: [ 'action1', 'render' ] }
            ],
            plugins: [
                tS,
                glS
            ]
        });
    }

} else {
    const warning = WEBGL.getWebGLErrorMessage();
    document.getElementById("container").appendChild(warning);
}

function renderScene(e) {
    for(let m=0;m<scenesArray.length;m++) {
        // Animate nodes
        for (let t = 0; t < sceneContents[m].length; t++) {
            let constructorName = Object.keys(sceneContents[m][t])[0];
            if (typeof sceneContents[m][t][constructorName][0].update != 'undefined') {
                sceneContents[m][t][constructorName][0].update(e, true);
            }
        }

        scenesArray[m].update(e);
    }

    if(STATS) {
        rS( 'frame' ).start();
        glS.start();
        rS( 'frame' ).start();
        rS( 'rAF' ).tick();
        rS( 'FPS' ).frame();
        rS( 'action1' ).start();
        /* Perform action #1 */
        rS( 'action1' ).end();
        rS( 'render' ).start();
        /* Perform render */
        rS( 'render' ).end();
        rS( 'frame' ).end();
        rS().update();
    }

    if(SIMULATE) {
        setTimeout( function() {
            requestAnimationFrame( renderScene );
        }, 1000 / 60 );
    } else {
        requestAnimationFrame(renderScene);
    }

    renderer.info.reset();
}

// Let's GO!
renderScene();

// Handle loading
var loadingVar = setInterval(loading, 20);
function loading() {
    if(LOADED >= (sceneContents[0].length)) {
        stopLoading();
    }
    $('#loader_text').html(((LOADED/(sceneContents[0].length))*100).toFixed(0)+'%');
}
function stopLoading() {
    clearInterval(loadingVar);
    $('body').addClass('loaded');
}

$(window).focus(function() {
    WINDOW_ACTIVE = true;
    if(MUSIC) {
        //sceneContents[0][4]['music'][0].play();
    }
});

$(window).blur(function() {
    WINDOW_ACTIVE = false;
    //sceneContents[0][4]['music'][0].stop();
});