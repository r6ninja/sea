$(document).ready(function() {
    $('#header-menu-btn').click(function(e) {
        $('html').toggleClass('is-menu-visible');
        $(this).toggleClass('is-selected');
    });

    $('#soundBars').click(function () {
        $('#soundBars .bar').each(function() {
            $(this).toggleClass('playing');
            $(this).toggleClass('hidebar');
        });
        $(this).toggleClass('playing');
        $('.play-btn').toggleClass('playing');

        MUSIC = !MUSIC;
        if(!MUSIC) {
            const event = new Event('pauseMusic');
            window.dispatchEvent(event);
        } else {
            const event = new Event('playMusic');
            window.dispatchEvent(event);
        }
    });

    $('.models li.bricks').click(function(e) {
        e.preventDefault();
        let id = $(this).data('id');
        const event = new CustomEvent("changeModel", {
            "detail": {id:id}
        });
        window.dispatchEvent(event);
    });

    $('.header-link').click(function(e) {
        $.scrollify.move($(this).attr("href"));
        $('html').removeClass('is-menu-visible');
        $('#header-menu-btn').removeClass('is-selected');
    });
});