/**
 * Setup the scene post processing effects
 */
import * as THREE from './3rdParty/three.module.js'
import {
    BlendFunction,
    BloomEffect, SelectiveBloomEffect,
    EffectComposer,
    SMAAEffect, SMAAImageLoader,
    EffectPass, KernelSize, ShaderPass, VignetteEffect, ScanlineEffect,
    RenderPass, Resizer, ToneMappingEffect, ToneMappingMode, DepthOfFieldEffect
} from "./3rdParty/postprocessing.esm.js";
import {CustomNoiseEffect} from "./3rdParty/effects_custom/CustomNoiseEffect.js";
import {HazeEffect} from "./3rdParty/effects_custom/HazeEffect.js";

import {GUI} from './3rdParty/dat.gui.module.js'
export const postShaders = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                //this._Initialize2();
                this.loadSMAAImages();
            }

            update(t, renderMe, top, bottom, sceneNum) {
                if(POST_PROCESSING && renderMe && this.loaded) {
                    this.render();

                    if(this.noiseEffect) {
                        if(typeof this.currentScene.effects.customNoise.settings.offSet !== 'undefined' && this.currentScene.effects.customNoise.settings.offSet) {
                            if (typeof FadeShaders_ !== 'undefined' && sceneNum === 1) {
                                let newTop = top / WINDOW_HEIGHT;
                                let newBottom = -((bottom - WINDOW_HEIGHT) / WINDOW_HEIGHT);
                                this.noiseEffect.setNewBottom(newTop);
                                this.noiseEffect.setNewTop(newBottom);
                            }
                        } else {
                            let modify = 0.0;
                            let direction = 1.0;
                            if (bottom >= WINDOW_HEIGHT) {
                                modify = (WINDOW_HEIGHT) - bottom;
                                modify = modify / (WINDOW_HEIGHT);
                                //console.log(bottom);
                            } else if (bottom < WINDOW_HEIGHT) {
                                modify = 1.0 - (bottom / (WINDOW_HEIGHT));
                                direction = 0.0;
                            }

                            this.noiseEffect.setBottom(-modify);
                            this.noiseEffect.setDirection(direction);
                        }
                    }

                    if(this.hazeEffect && this.playScene) {
                        this.hazeEffect.uniforms.get("iTime").value += this.clock.getDelta();
                    }
                }
            }

            // Renders the scene either with or without post-processing shaders
            render() {
                //this.currentScene.renderer.setRenderTarget(this.currentScene.renderTarget);
                //this.currentScene.renderer.render(this.currentScene.rtScene, this.currentScene.rtCamera);
                //this.currentScene.renderer.setRenderTarget(null);

                if(this.composer) {
                    this.currentScene.renderer.clear();
                    this.composer.render();
                }
                    //this.composer.render(this.clock.getDelta());
            }

            // Calls the applicable functions to animate the scene
            animate(e) {
                requestAnimationFrame(this.animateMe);
                this.update(e);
            }

            loadSMAAImages() {
                let that = this;
                this.assets = new Map();
                const loadingManager = new THREE.LoadingManager();
                const smaaImageLoader = new SMAAImageLoader(loadingManager);

                return new Promise((resolve, reject) => {
                    loadingManager.onLoad = () => resolve(that.assets);
                    loadingManager.onError = reject;
                    smaaImageLoader.load(([search, area]) => {
                        this.assets.set("smaa-search", search);
                        this.assets.set("smaa-area", area);
                        this._Initialize();
                        this.loaded = true;
                    });
                });
            }

            _Initialize() {
                this.clock = new THREE.Clock();
                this.composer = new EffectComposer(this.currentScene.renderer, { frameBufferType: THREE.HalfFloatType });
                this.playScene = false;

                // General anti-aliasing effect
                const smaaEffect = new SMAAEffect(
                    this.assets.get("smaa-search"),
                    this.assets.get("smaa-area")
                );
                smaaEffect.edgeDetectionMaterial.setEdgeDetectionThreshold(0.02);

                let activeProcessing = [];
                activeProcessing.push(smaaEffect);

                // Custom noise effect
                if (this.currentScene.effects.customNoise.on) {
                    const noiseOptions = {
                        blendFunction : (this.currentScene.effects.customNoise.settings.blendFunction) ? this.currentScene.effects.customNoise.settings.blendFunction : BlendFunction.SCREEN,
                        premultiply: (typeof this.currentScene.effects.customNoise.settings.premultiply !== 'undefined') ? this.currentScene.effects.customNoise.settings.premultiply : true,
                        opacity: (this.currentScene.effects.customNoise.settings.opacity) ? this.currentScene.effects.customNoise.settings.opacity : 0.75,
                        fullScreen: (typeof this.currentScene.effects.customNoise.settings.fullScreen !== 'undefined') ? this.currentScene.effects.customNoise.settings.fullScreen : false,
                        fadeBottom: (typeof this.currentScene.effects.customNoise.settings.fadeBottom !== 'undefined') ? this.currentScene.effects.customNoise.settings.fadeBottom : false,
                        fadeTop: (typeof this.currentScene.effects.customNoise.settings.fadeTop !== 'undefined') ? this.currentScene.effects.customNoise.settings.fadeTop : false,
                        emptyFadeBottom: (typeof this.currentScene.effects.customNoise.settings.emptyFadeBottom !== 'undefined') ? this.currentScene.effects.customNoise.settings.emptyFadeBottom : false,
                        emptyFadeTop: (typeof this.currentScene.effects.customNoise.settings.emptyFadeTop !== 'undefined') ? this.currentScene.effects.customNoise.settings.emptyFadeTop : false,
                        offSet: (typeof this.currentScene.effects.customNoise.settings.offSet !== 'undefined') ? this.currentScene.effects.customNoise.settings.offSet : false
                    };
                    this.noiseEffect = new CustomNoiseEffect(noiseOptions);

                    activeProcessing.push(this.noiseEffect);
                }

                // Custom haze effect
                if (this.currentScene.effects.haze.on) {
                    const hazeOptions = {
                        blendFunction : (this.currentScene.effects.haze.settings.blendFunction) ? this.currentScene.effects.haze.settings.blendFunction : BlendFunction.SCREEN,
                        premultiply: (typeof this.currentScene.effects.haze.settings.premultiply !== 'undefined') ? this.currentScene.effects.haze.settings.premultiply : true,
                        opacity: (this.currentScene.effects.haze.settings.opacity) ? this.currentScene.effects.haze.settings.opacity : 0.75,
                        fullScreen: (typeof this.currentScene.effects.haze.settings.fullScreen !== 'undefined') ? this.currentScene.effects.haze.settings.fullScreen : false,
                        fadeBottom: (typeof this.currentScene.effects.haze.settings.fadeBottom !== 'undefined') ? this.currentScene.effects.haze.settings.fadeBottom : false,
                        fadeTop: (typeof this.currentScene.effects.haze.settings.fadeTop !== 'undefined') ? this.currentScene.effects.haze.settings.fadeTop : false,
                        emptyFadeBottom: (typeof this.currentScene.effects.haze.settings.emptyFadeBottom !== 'undefined') ? this.currentScene.effects.haze.settings.emptyFadeBottom : false,
                        emptyFadeTop: (typeof this.currentScene.effects.haze.settings.emptyFadeTop !== 'undefined') ? this.currentScene.effects.haze.settings.emptyFadeTop : false,
                        offSet: (typeof this.currentScene.effects.haze.settings.offSet !== 'undefined') ? this.currentScene.effects.haze.settings.offSet : false,
                        iTime: 0.0
                    };
                    this.hazeEffect = new HazeEffect(hazeOptions);

                    activeProcessing.push(this.hazeEffect);
                }

                // Depth of field effect
                if (this.currentScene.effects.bokeh.on) {
                    const DOFOptions = {
                        blendFunction: (this.currentScene.effects.bokeh.settings.blendFunction) ? this.currentScene.effects.bokeh.settings.blendFunction : BlendFunction.NORMAL,
                        focusDistance: (this.currentScene.effects.bokeh.settings.focusDistance) ? this.currentScene.effects.bokeh.settings.focusDistance : 0.0,
                        focalLength: (this.currentScene.effects.bokeh.settings.focalLength) ? this.currentScene.effects.bokeh.settings.focalLength : 0.037,
                        bokehScale: (this.currentScene.effects.bokeh.settings.bokehScale) ? this.currentScene.effects.bokeh.settings.bokehScale : 5.0,
                        height: 480
                    }
                    this.DOFEffect = new DepthOfFieldEffect(this.currentScene.camera, DOFOptions);
                    this.currentScene.DOFEffect = this.DOFEffect;
                    this.currentScene.cocMaterial = this.DOFEffect.circleOfConfusionMaterial;

                    activeProcessing.push(this.DOFEffect);
                }

                // Bloom effect
                if(this.currentScene.effects.bloom.on) {
                    const bloomOptions = {
                        blendFunction: (this.currentScene.effects.bloom.settings.blendFunction) ? this.currentScene.effects.bloom.settings.blendFunction : BlendFunction.SCREEN,
                        kernelSize: (this.currentScene.effects.bloom.settings.kernelSize) ? this.currentScene.effects.bloom.settings.kernelSize : KernelSize.SMALL,
                        intensity: (this.currentScene.effects.bloom.settings.intensity) ? this.currentScene.effects.bloom.settings.intensity : 4.0,
                        luminanceThreshold: (this.currentScene.effects.bloom.settings.luminanceThreshold) ? this.currentScene.effects.bloom.settings.luminanceThreshold : 0.0,
                        luminanceSmoothing: (this.currentScene.effects.bloom.settings.luminanceSmoothing) ? this.currentScene.effects.bloom.settings.luminanceSmoothing : 0.2,
                        height: (this.currentScene.effects.bloom.settings.height) ? this.currentScene.effects.bloom.settings.height : 720,
                        resolutionScale: (this.currentScene.effects.bloom.settings.resolutionScale) ? this.currentScene.effects.bloom.settings.resolutionScale : 0.5
                    }
                    this.bloomEffect = new BloomEffect(bloomOptions);

                    activeProcessing.push(this.bloomEffect);
                } else if(this.currentScene.effects.bloomSelect.on) {
                    const bloomSelectOptions = {
                        blendFunction: (this.currentScene.effects.bloomSelect.settings.blendFunction) ? this.currentScene.effects.bloomSelect.settings.blendFunction : BlendFunction.SCREEN,
                        kernelSize: (this.currentScene.effects.bloomSelect.settings.kernelSize) ? this.currentScene.effects.bloomSelect.settings.kernelSize : KernelSize.SMALL,
                        intensity: (this.currentScene.effects.bloomSelect.settings.intensity) ? this.currentScene.effects.bloomSelect.settings.intensity : 4.0,
                        luminanceThreshold: (this.currentScene.effects.bloomSelect.settings.luminanceThreshold) ? this.currentScene.effects.bloomSelect.settings.luminanceThreshold : 0.1,
                        luminanceSmoothing: (this.currentScene.effects.bloomSelect.settings.luminanceSmoothing) ? this.currentScene.effects.bloomSelect.settings.luminanceSmoothing : 0.2,
                        height: (this.currentScene.effects.bloomSelect.settings.height) ? this.currentScene.effects.bloomSelect.settings.height : 720
                    };
                    this.selectiveBloomEffect = new SelectiveBloomEffect(this.currentScene.scene, this.currentScene.camera, bloomSelectOptions);
                    this.currentScene.selectiveBloomEffect = this.selectiveBloomEffect;

                    activeProcessing.push(this.selectiveBloomEffect);

                    this.selectiveBloomEffect.blurPass.scale = (this.currentScene.effects.bloomSelect.settings.blurScale) ? this.currentScene.effects.bloomSelect.settings.blurScale : 1;
                }

                // Vignette effect
                if(this.currentScene.effects.vignette.on) {
                    const vignetteOptions = {
                        offset: 0.25,
                        darkness: 0.5,
                        blendFunction: BlendFunction.NORMAL
                    }
                    this.vignetteEffect = new VignetteEffect(vignetteOptions);

                    activeProcessing.push(this.vignetteEffect);
                }

                // Tonemapping effect
                if(this.currentScene.effects.toneMapping.on) {
                    const toneOptions = {
                        mode: ToneMappingMode.ACES_FILMIC
                    };
                    this.toneMappingEffect = new ToneMappingEffect(toneOptions);

                    activeProcessing.push(this.toneMappingEffect);
                }

                // ScanLines effect
                if(this.currentScene.effects.scanlines.on) {
                    this.scanlineEffect = new ScanlineEffect({
                        blendFunction: (this.currentScene.effects.scanlines.settings.blendFunction) ? this.currentScene.effects.scanlines.settings.blendFunction : BlendFunction.OVERLAY,
                        density: (this.currentScene.effects.scanlines.settings.density) ? this.currentScene.effects.scanlines.settings.density : 2.0
                    });

                    activeProcessing.push(this.scanlineEffect);
                }

                /*let t = [
                    smaaEffect,
                    (this.currentScene.effects.bokeh.on) ? this.DOFEffect : null,
                    (this.currentScene.effects.bloom.on) ? this.bloomEffect : null,
                    (this.currentScene.effects.bloomSelect.on) ? this.selectiveBloomEffect : null,
                    (this.currentScene.effects.vignette.on) ? this.vignetteEffect : null,
                    (this.currentScene.effects.toneMapping.on) ? this.toneMappingEffect : null,
                    (this.currentScene.effects.customNoise.on) ? this.noiseEffect : null,
                    (this.currentScene.effects.scanlines.on) ? this.scanlineEffect : null
                ];*/

                const renderPass = new RenderPass(this.currentScene.scene, this.currentScene.camera);
                this.effectPass = new EffectPass(
                    this.currentScene.camera,
                    activeProcessing
                );

                this.composer.addPass(renderPass);
                this.composer.addPass(this.effectPass);

                if(this.currentScene.effects.gui) this.gui();

                this.events();
                LOADED++;
            }

            events() {
                let that= this;
                window.addEventListener('unlock', function() {
                    that.playScene = true;
                });
            }

            gui() {
                let menu = new GUI();
                const renderer = this.composer.getRenderer();

                if(this.noiseEffect) {
                    const effectPass = this.noiseEffect;
                    const blendMode = effectPass.blendMode;
                    const params = {
                        "opacity": blendMode.opacity.value,
                        "blend mode": blendMode.blendFunction,
                        "fullscreen": effectPass.getFullScreen(),
                    };

                    menu.add(params, "opacity").min(0.0).max(1.0).step(0.01).onChange((value) => {
                        blendMode.opacity.value = value;
                    });
                    menu.add(params, "blend mode", BlendFunction).onChange((value) => {
                        blendMode.setBlendFunction(Number(value));
                    });
                    menu.add(params, "fullscreen").onChange((value) => {
                        effectPass.setFullScreen(value);
                    });
                }

                if(this.scanlineEffect) {
                    const effectPass = this.scanlineEffect;
                    const blendMode = effectPass.blendMode;
                    const params = {
                        "blend mode": blendMode.blendFunction,
                        "density": effectPass.density,
                    };

                    menu.add(params, "density").min(0.0).max(10.0).step(0.1).onChange((value) => {
                        effectPass.setDensity(value);
                    });
                    menu.add(params, "blend mode", BlendFunction).onChange((value) => {
                        blendMode.setBlendFunction(Number(value));
                    });
                }

                if(this.DOFEffect) {
                    const effectPass = this.effectPass;
                    const depthOfFieldEffect = this.DOFEffect;
                    const cocMaterial = depthOfFieldEffect.circleOfConfusionMaterial;
                    const blendMode = depthOfFieldEffect.blendMode;

                    const RenderMode = {
                        DEFAULT: 0,
                        DEPTH: 1,
                        COC: 2
                    };

                    const params = {
                        "coc": {
                            "edge blur kernel": depthOfFieldEffect.blurPass.kernelSize,
                            "focus": cocMaterial.uniforms.focusDistance.value,
                            "focal length": cocMaterial.uniforms.focalLength.value
                        },
                        "render mode": RenderMode.DEFAULT,
                        "resolution": depthOfFieldEffect.resolution.height,
                        "bokeh scale": depthOfFieldEffect.bokehScale,
                        "opacity": blendMode.opacity.value,
                        "blend mode": blendMode.blendFunction
                    };

                    function toggleRenderMode() {
                        const mode = Number(params["render mode"]);
                        effectPass.encodeOutput = (mode === RenderMode.DEFAULT);
                        effectPass.renderToScreen = (mode !== RenderMode.DEFAULT);
                    }
                    menu.add(params, "render mode", RenderMode).onChange(toggleRenderMode);

                    menu.add(params, "resolution", [240, 360, 480, 720, 1080]).onChange((value) => {
                        depthOfFieldEffect.resolution.height = Number(value);
                    });

                    menu.add(params, "bokeh scale").min(1.0).max(5.0).step(0.001).onChange((value) => {
                        depthOfFieldEffect.bokehScale = value;
                    });

                    let folder = menu.addFolder("Circle of Confusion");

                    folder.add(params.coc, "edge blur kernel", KernelSize).onChange((value) => {
                        depthOfFieldEffect.blurPass.kernelSize = Number(value);
                    });

                    folder.add(params.coc, "focus").min(0.0).max(1.0).step(0.001).onChange((value) => {
                        cocMaterial.uniforms.focusDistance.value = value;
                    });

                    folder.add(params.coc, "focal length").min(0.0).max(1.0).step(0.0001).onChange((value) => {
                        cocMaterial.uniforms.focalLength.value = value;
                    });

                    folder.open();

                    menu.add(params, "opacity").min(0.0).max(1.0).step(0.01).onChange((value) => {
                        blendMode.opacity.value = value;
                    });

                    menu.add(params, "blend mode", BlendFunction).onChange((value) => {
                        blendMode.setBlendFunction(Number(value));
                    });
                }

                if(this.selectiveBloomEffect || this.bloomEffect) {
                    const passA = this.effectPass;
                    const effectA = (this.bloomEffect) ? this.bloomEffect : this.selectiveBloomEffect;
                    const blendModeA = effectA.blendMode;

                    const params = {
                        "resolution": effectA.resolution.height,
                        "kernel size": effectA.blurPass.kernelSize,
                        "blur scale": effectA.blurPass.scale,
                        "intensity": effectA.intensity,
                        "luminance": {
                            "filter": effectA.luminancePass.enabled,
                            "threshold": effectA.luminanceMaterial.threshold,
                            "smoothing": effectA.luminanceMaterial.smoothing
                        },
                        "selection": {
                            "enabled": passA.enabled,
                            "inverted": effectA.inverted,
                            "ignore bg": effectA.ignoreBackground
                        },
                        "opacity": blendModeA.opacity.value,
                        "blend mode": blendModeA.blendFunction
                    };

                    menu.add(params, "resolution", [240, 360, 480, 720, 1080]).onChange((value) => {
                        effectA.resolution.height = effectA.resolution.height = Number(value);
                    });

                    menu.add(params, "kernel size", KernelSize).onChange((value) => {
                        effectA.blurPass.kernelSize = effectA.blurPass.kernelSize = Number(value);
                    });

                    menu.add(params, "blur scale").min(0.0).max(1.0).step(0.01).onChange((value) => {
                        effectA.blurPass.scale = effectA.blurPass.scale = Number(value);
                    });

                    menu.add(params, "intensity").min(0.0).max(10.0).step(0.01).onChange((value) => {
                        effectA.intensity = effectA.intensity = Number(value);
                    });

                    let folder = menu.addFolder("Luminance");

                    folder.add(params.luminance, "filter").onChange((value) => {
                        effectA.luminancePass.enabled = effectA.luminancePass.enabled = value;
                    });

                    folder.add(params.luminance, "threshold").min(0.0).max(1.0).step(0.001).onChange((value) => {
                        effectA.luminanceMaterial.threshold = effectA.luminanceMaterial.threshold = Number(value);
                    });

                    folder.add(params.luminance, "smoothing").min(0.0).max(1.0).step(0.001).onChange((value) => {
                        effectA.luminanceMaterial.smoothing = effectA.luminanceMaterial.smoothing = Number(value);
                    });
                    folder.open();
                    folder = menu.addFolder("Selection");

                    folder.add(params.selection, "enabled").onChange((value) => {

                        passA.enabled = value;
                        passA.enabled = !passA.enabled;

                        if (passA.enabled) {
                            renderer.domElement.addEventListener("mousedown", this);
                        } else {
                            renderer.domElement.removeEventListener("mousedown", this);
                        }
                    });

                    if(this.selectiveBloomEffect) {
                        folder.add(params.selection, "inverted").onChange((value) => {
                            effectA.inverted = value;
                        });

                        folder.add(params.selection, "ignore bg").onChange((value) => {
                            effectA.ignoreBackground = value;
                        });
                    }
                    folder.open();

                    menu.add(params, "opacity").min(0.0).max(1.0).step(0.01).onChange((value) => {
                        blendModeA.opacity.value = blendModeA.opacity.value = value;
                    });
                    menu.add(params, "blend mode", BlendFunction).onChange((value) => {
                        blendModeA.setBlendFunction(Number(value));
                    });
                    menu.add(passA, "dithering").onChange((value) => {
                        passA.dithering = value;
                    });
                }

                if(this.toneMappingEffect) {
                    const effect = this.toneMappingEffect;
                    const blendMode = effect.blendMode;
                    const adaptiveLuminancePass = effect.adaptiveLuminancePass;
                    const adaptiveLuminanceMaterial = adaptiveLuminancePass.getFullscreenMaterial();

                    const params = {
                        "mode": effect.getMode(),
                        "exposure": renderer.toneMappingExposure,
                        "resolution": effect.resolution,
                        "white point": effect.uniforms.get("whitePoint").value,
                        "middle grey": effect.uniforms.get("middleGrey").value,
                        "average lum": effect.uniforms.get("averageLuminance").value,
                        "min lum": adaptiveLuminanceMaterial.uniforms.minLuminance.value,
                        "adaptation rate": adaptiveLuminancePass.adaptationRate,
                        "opacity": blendMode.opacity.value,
                        "blend mode": blendMode.blendFunction
                    };

                    menu.add(params, "mode", ToneMappingMode).onChange((value) => {
                        effect.setMode(Number(value));
                    });

                    menu.add(params, "exposure").min(0.0).max(2.0).step(0.001).onChange((value) => {
                        renderer.toneMappingExposure = value;
                    });

                    let f = menu.addFolder("Reinhard (Modified)");

                    f.add(params, "white point").min(2.0).max(32.0).step(0.01).onChange((value) => {
                        effect.uniforms.get("whitePoint").value = value;
                    });

                    f.add(params, "middle grey").min(0.0).max(1.0).step(0.0001).onChange((value) => {
                        effect.uniforms.get("middleGrey").value = value;
                    });

                    f.add(params, "average lum").min(0.0001).max(1.0).step(0.0001).onChange((value) => {
                        effect.uniforms.get("averageLuminance").value = value;
                    });

                    f.open();

                    f = menu.addFolder("Reinhard (Adaptive)");

                    f.add(params, "resolution", [64, 128, 256, 512]).onChange((value) => {
                        effect.resolution = Number(value);
                    });

                    f.add(params, "adaptation rate").min(0.001).max(3.0).step(0.001).onChange((value) => {
                        adaptiveLuminancePass.adaptationRate = value;
                    });

                    f.add(params, "min lum").min(0.001).max(1.0).step(0.001).onChange((value) => {
                        adaptiveLuminanceMaterial.uniforms.minLuminance.value = value;
                    });

                    f.open();

                    menu.add(params, "opacity").min(0.0).max(1.0).step(0.01).onChange((value) => {
                        blendMode.opacity.value = value;
                    });

                    menu.add(params, "blend mode", BlendFunction).onChange((value) => {
                        blendMode.setBlendFunction(Number(value));
                    });
                }
            }
        }
    }
})();