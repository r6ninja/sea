export const instructions = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this._Initialize();
            }

            _Initialize() {
                this.blocker =  document.getElementById('blocker');
                this.instructions = document.getElementById('instructions');
                this.events();

                LOADED++;
            }

            // Initialise object specific events
            events() {
                var that = this;
                this.instructions.addEventListener('click', function () {
                    const event = new Event('unlock');
                    window.dispatchEvent(event);

                    that.instructions.style.display = 'none';
                    that.blocker.style.display = 'none';
                }, false);

                window.addEventListener('unLockControls', function () {
                    that.blocker.style.display = 'block';
                    that.instructions.style.display = '';
                }, false);
            }
        }
    }
})();