const fragmentShader = `
uniform float bottom;
uniform float newBottom;
uniform float newTop;
uniform float direction;
uniform bool fadeBottom;
uniform bool fadeTop;
uniform bool emptyFadeBottom;
uniform bool emptyFadeTop;
uniform bool fullScreen;
uniform bool offSet;
varying vec4 vPosT;

float random( vec2 p ) {
        vec2 K1 = vec2(
          23.14069263277926, // e^pi (Gelfond's constant)
          2.665144142690225 // 2^sqrt(2) (Gelfondâ€“Schneider constant)
        );
        return fract( cos( dot(p,K1) ) * 12345.6789 );
      }
void mainImage(const in vec4 inputColor, const in vec2 uv, out vec4 outputColor) {
	vec3 noise = vec3(rand(uv * time));

	#ifdef PREMULTIPLY
		vec4 color = inputColor;
        vec2 uvRandom = vUv;
        uvRandom.y *= random(vec2(uvRandom.y,0.0));
        //float tColor = random(uvRandom)*0.075;
        float tColor = random(uvRandom)*0.085;
        color.rgb += tColor;
            
        // OverWrite alpha
        //color.a = 0.6;
        float currBottom = vUv.y + bottom;
        /*if(currBottom >= 0.1) {
            color.a = 1.0;
        }*/
        
        if(fullScreen) {
            if(offSet) {
                vec2 vCoords = vPosT.xy;
                vCoords /= vPosT.w;
                vCoords = vCoords * 0.5 + 0.5;
                float currTop = vCoords.y - newTop;
                float currBottom = vCoords.y + newBottom;
                
                if(currTop >= 1.0) {
                    //color.g = 1.0;
                    //color.a = (1.2-currTop)*10.0;
                    //color.a = 0.66;
                    if(fadeTop) {
                        color.a = (1.2-currTop)*10.0;
                    } else {
                       if(!emptyFadeTop) {
                            color.a = 1.0;
                       }
                    }
                } else if(currBottom+0.2 <= 0.0) {
                    //color.b = 1.0;
                    //color.a = (0.2+currBottom)*10.0;
                    if(fadeBottom) {
                        color.a = (0.2+currBottom)*10.0;
                        //color.a = 0.66;
                    } else {
                        if(!emptyFadeBottom) {
                            color.a = 1.0;
                       }
                    }
                } else {
                    color.a = 1.0;
                }
            } else {
                if(currBottom > 0.1 && currBottom < 0.9) {
                    color.a = 1.0;
                } else if(currBottom <= 0.1) {
                    if(fadeBottom) {
                        color.a = currBottom*10.0;
                        //color.a = 0.66;
                    } else {
                        if(!emptyFadeBottom) {
                            color.a = 1.0;
                       }
                    }
                } else if(currBottom >= 0.9) {
                    if(fadeTop) {
                        color.a = (1.0-currBottom)*10.0;
                    } else {
                       if(!emptyFadeTop) {
                            color.a = 1.0;
                       }
                    }
                }
            }
        }
            
        outputColor = vec4(color);
	#else
		outputColor = vec4(noise, inputColor.a);
	#endif
}
`

export { fragmentShader }