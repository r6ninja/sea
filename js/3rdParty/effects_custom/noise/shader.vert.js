const vertexShader = `uniform vec2 offset;

varying vec2 vUvR;
varying vec2 vUvB;
uniform float bottom;
uniform float newBottom;
uniform float newTop;
uniform float direction;
uniform bool fadeBottom;
uniform bool fadeTop;
uniform bool emptyFadeBottom;
uniform bool emptyFadeTop;
uniform bool offSet;
varying vec4 vPosT;

void mainSupport(const in vec2 uv) {

	vUvR = uv + offset;
	vUvB = uv - offset;
	
	vPosT = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

}
`

export { vertexShader }