import {
	BlendFunction,Effect
} from "../postprocessing.esm.js";

import { fragmentShader } from "./noise/shader.frag.js";
import {vertexShader} from "./noise/shader.vert.js";
import {Uniform} from "../three.module.js";
/**
 * A noise effect.
 */

export class CustomNoiseEffect extends Effect {

	/**
	 * Constructs a new noise effect.
	 *
	 * @param {Object} [options] - The options.
	 * @param {BlendFunction} [options.blendFunction=BlendFunction.SCREEN] - The blend function of this effect.
	 * @param {Boolean} [options.premultiply=false] - Whether the noise should be multiplied with the input color.
	 */

	constructor({
					blendFunction = BlendFunction.SCREEN,
					premultiply = false,
					bottom=0.0,
					direction=1.0,
					opacity=0.75,
					fullScreen=true,
					fadeBottom=false,
					fadeTop=false,
					emptyFadeBottom=false,
					emptyFadeTop=false,
					offSet=false
	} = {}) {

		super("CustomNoiseEffect", fragmentShader, {
			vertexShader,
			blendFunction,
			uniforms: new Map([
				["newBottom", new Uniform(0.0)],
				["newTop", new Uniform(0.0)],
				["bottom", new Uniform(bottom)],
				["direction", new Uniform(direction)],
				["fullScreen", new Uniform(fullScreen)],
				["fadeBottom", new Uniform(fadeBottom)],
				["fadeTop", new Uniform(fadeTop)],
				["emptyFadeBottom", new Uniform(emptyFadeBottom)],
				["emptyFadeTop", new Uniform(emptyFadeTop)],
				["offSet", new Uniform(offSet)]
			])
		});

		this.premultiply = premultiply;
		this.blendMode.opacity.value = opacity;
	}

	setNewBottom(bottom) {
		this.uniforms.get("newBottom").value = bottom;
	}

	setNewTop(top) {
		this.uniforms.get("newTop").value = top;
	}

	setBottom(bottom) {
		this.uniforms.get("bottom").value = bottom;
	}

	setDirection(direction) {
		this.uniforms.get("direction").value = direction;
	}

	setFullScreen(fullScreen) {
		this.uniforms.get("fullScreen").value = fullScreen;
	}

	getFullScreen() {
		return this.uniforms.get("fullScreen").value;
	}

	/**
	 * Indicates whether the noise should be multiplied with the input color.
	 *
	 * @type {Boolean}
	 */

	get premultiply() {

		return this.defines.has("PREMULTIPLY");

	}

	/**
	 * Enables or disables noise premultiplication.
	 *
	 * @type {Boolean}
	 */

	set premultiply(value) {

		if(this.premultiply !== value) {

			if(value) {

				this.defines.set("PREMULTIPLY", "1");

			} else {

				this.defines.delete("PREMULTIPLY");

			}

			this.setChanged();

		}

	}

}
