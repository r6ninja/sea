const fragmentShader = `
uniform float iTime;

void mainUv(inout vec2 uv) {
    // Time varying pixel color
    float jacked_time = 10.0*iTime;
    const vec2 scale = vec2(5.);

    uv += 0.001*sin(scale*jacked_time + length( uv )*50.0);
}

void mainImage(in vec4 inputColor, in vec2 uv, out vec4 outputColor) {
  outputColor = inputColor;
}
`

export { fragmentShader }