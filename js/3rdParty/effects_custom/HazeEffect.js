import {
	BlendFunction,Effect
} from "../postprocessing.esm.js";

import {fragmentShader} from "./haze/shader.frag.js";
import {vertexShader} from "./haze/shader.vert.js";
import {Uniform} from "../three.module.js";
/**
 * A haze effect.
 */

export class HazeEffect extends Effect {

	/**
	 * Constructs a new haze effect.
	 *
	 * @param {Object} [options] - The options.
	 * @param {BlendFunction} [options.blendFunction=BlendFunction.SCREEN] - The blend function of this effect.
	 * @param {Boolean} [options.premultiply=false] - Whether the haze should be multiplied with the input color.
	 */

	constructor({
					blendFunction = BlendFunction.SCREEN,
					premultiply = false,
					iTime=0.0,
	} = {}) {

		super("HazeEffect", fragmentShader, {
			vertexShader,
			blendFunction,
			uniforms: new Map([
				["iTime", new Uniform(iTime)]
			])
		});

		this.premultiply = premultiply;
	}

	/**
	 * Indicates whether the haze should be multiplied with the input color.
	 *
	 * @type {Boolean}
	 */

	get premultiply() {

		return this.defines.has("PREMULTIPLY");

	}

	/**
	 * Enables or disables haze premultiplication.
	 *
	 * @type {Boolean}
	 */

	set premultiply(value) {

		if(this.premultiply !== value) {

			if(value) {

				this.defines.set("PREMULTIPLY", "1");

			} else {

				this.defines.delete("PREMULTIPLY");

			}

			this.setChanged();

		}

	}

}
