﻿/**
 * Original work:
 * @author David Li / http://david.li/waves/
 *
 * Three.js version:
 * @author Aleksandr Albert / http://www.routter.co.tt
 * 
 * Modified:
 * @author jbouny / https://github.com/fft-ocean
 */
 
// Author: Aleksandr Albert
// Website: www.routter.co.tt

// Description: A deep water ocean shader set
// based on an implementation of a Tessendorf Waves
// originally presented by David Li ( www.david.li/waves )

// The general method is to apply shaders to simulation Framebuffers
// and then sample these framebuffers when rendering the ocean mesh

// The set uses 7 shaders:

// -- Simulation shaders
// [1] ocean_sim_vertex         -> Vertex shader used to set up a 2x2 simulation plane centered at (0,0)
// [2] ocean_subtransform       -> Fragment shader used to subtransform the mesh (generates the displacement map)
// [3] ocean_initial_spectrum   -> Fragment shader used to set intitial wave frequency at a texel coordinate
// [4] ocean_phase              -> Fragment shader used to set wave phase at a texel coordinate
// [5] ocean_spectrum           -> Fragment shader used to set current wave frequency at a texel coordinate
// [6] ocean_normal             -> Fragment shader used to set face normals at a texel coordinate

// -- Rendering Shader
// [7] ocean_main               -> Vertex and Fragment shader used to create the final render

import * as THREE from './three.module.js';

var OceanShaders = {};

OceanShaders["screenplane_pars_vertex"] = [

	'const float infinite = 150000.0;',
	'const float screenScale = 1.2;',
	'const vec3 groundNormal = vec3( 0.0, 1.0, 0.0 );',
	'const float groundHeight = 0.0;',

	'varying vec3 vCamPosition;',

	'vec3 interceptPlane( in vec3 source, in vec3 dir, in vec3 normal, float height )',
	'{',
	// Compute the distance between the source and the surface, following a ray, then return the intersection
	// http://www.cs.rpi.edu/~cutler/classes/advancedgraphics/S09/lectures/11_ray_tracing.pdf
	'	float distance = ( - height - dot( normal, source ) ) / dot( normal, dir );',
	'	if( distance < 0.0 )',
	'		return source + dir * distance;',
	'	else ',
	'		return - ( vec3( source.x, height, source.z ) + vec3( dir.x, height, dir.z ) * infinite );',
	'}',

	'mat3 getRotation()',
	'{',
	// Extract the 3x3 rotation matrix from the 4x4 view matrix
	'	return mat3( ',
	'		viewMatrix[0].xyz,',
	'		viewMatrix[1].xyz,',
	'		viewMatrix[2].xyz',
	'	);',
	'}',

	'vec3 getCameraPos( in mat3 rotation )',
	'{',
	// Xc = R * Xw + t
	// c = - R.t() * t <=> c = - t.t() * R
	'	return - viewMatrix[3].xyz * rotation;',
	'}',

	'vec2 getImagePlan()',
	'{',
	// Extracting aspect and focal from projection matrix:
	// P = | e   0       0   0 |
	//     | 0   e/(h/w) 0   0 |
	//     | 0   0       .   . |
	//     | 0   0       -1  0 |
	'	float focal = projectionMatrix[0].x;',
	'	float aspect = projectionMatrix[1].y;',

	// Fix coordinate aspect and scale
	'	return vec2( ( uv.x - 0.5 ) * screenScale * aspect, ( uv.y - 0.5 ) * screenScale * focal );',
	'}',

	'vec3 getCamRay( in mat3 rotation, in vec2 screenUV )',
	'{',
	// Compute camera ray then rotate it in order to get it in world coordinate
	'	return vec3( screenUV.x, screenUV.y, projectionMatrix[0].x ) * rotation;',
	'}',

	'vec3 computeProjectedPosition()',
	'{',
	// Extract camera position and rotation from the model view matrix
	'	mat3 cameraRotation = getRotation();',
	'	vec3 camPosition = getCameraPos( cameraRotation );',
	'	vCamPosition = camPosition;',

	// Return the intersection between the camera ray and a given plane
	'	if( camPosition.y < groundHeight )',
	'		return vec3( 0.0, 0.0, 0.0 );',

	// Extract coordinate of the vertex on the image plan
	'	vec2 screenUV = getImagePlan() ;',

	// Compute the ray from camera to world
	'	vec3 ray = getCamRay( cameraRotation, screenUV );',

	'	vec3 finalPos = interceptPlane( camPosition, ray, groundNormal, groundHeight );',

	'	float distance = length( finalPos );',
	'	if( distance > infinite )',
	'		finalPos *= infinite / distance;',

	'	return finalPos;',
	'}'

].join('\n');

OceanShaders["screenplane_vertex"] = [
	'vec4 screenPlaneWorldPosition = vec4( computeProjectedPosition(), 1.0 );',
].join('\n');

OceanShaders["screenplane_pars_fragment"] = [
	'varying vec3 vCamPosition;'
].join('\n');

OceanShaders['ocean_sim_vertex'] = {
	varying: {
		"vUV": { type: "v2" }
	},
	vertexShader: [
		'varying vec2 vUV;',

		'void main (void) {',
			'vUV = position.xy * 0.5 + 0.5;',
			'gl_Position = vec4(position, 1.0 );',
		'}'
	].join('\n')
};
OceanShaders['ocean_subtransform'] = {
	uniforms: {
		"u_input": { type: "t", value: null },
		"u_transformSize": { type: "f", value: 512.0 },
		"u_subtransformSize": { type: "f", value: 250.0 }
	},
	varying: {
		"vUV": { type: "v2" }
	},
	fragmentShader: [
		//GPU FFT using a Stockham formulation

		'const float PI = 3.14159265359;',

		'uniform sampler2D u_input;',
		'uniform float u_transformSize;',
		'uniform float u_subtransformSize;',

		'varying vec2 vUV;',
		
		'vec2 multiplyComplex (vec2 a, vec2 b) {',
			'return vec2(a[0] * b[0] - a[1] * b[1], a[1] * b[0] + a[0] * b[1]);',
		'}',

		'void main (void) {',
			'#ifdef HORIZONTAL',
				'float index = vUV.x * u_transformSize - 0.5;',
			'#else',
				'float index = vUV.y * u_transformSize - 0.5;',
			'#endif',

			'float evenIndex = floor(index / u_subtransformSize) * (u_subtransformSize * 0.5) + mod(index, u_subtransformSize * 0.5);',

			//transform two complex sequences simultaneously
			'#ifdef HORIZONTAL',
				'vec4 even = texture2D(u_input, vec2(evenIndex + 0.5, gl_FragCoord.y) / u_transformSize).rgba;',
				'vec4 odd = texture2D(u_input, vec2(evenIndex + u_transformSize * 0.5 + 0.5, gl_FragCoord.y) / u_transformSize).rgba;',
			'#else',
				'vec4 even = texture2D(u_input, vec2(gl_FragCoord.x, evenIndex + 0.5) / u_transformSize).rgba;',
				'vec4 odd = texture2D(u_input, vec2(gl_FragCoord.x, evenIndex + u_transformSize * 0.5 + 0.5) / u_transformSize).rgba;',
			'#endif',

			'float twiddleArgument = -2.0 * PI * (index / u_subtransformSize);',
			'vec2 twiddle = vec2(cos(twiddleArgument), sin(twiddleArgument));',

			'vec2 outputA = even.xy + multiplyComplex(twiddle, odd.xy);',
			'vec2 outputB = even.zw + multiplyComplex(twiddle, odd.zw);',

			'gl_FragColor = vec4(outputA, outputB);',
		'}'
	].join('\n')
};
OceanShaders['ocean_initial_spectrum'] = {
	uniforms: {
		"u_wind": { type: "v2", value: new THREE.Vector2(10.0, 10.0) },
		"u_resolution": { type: "f", value: 512.0 },
		"u_size": { type: "f", value: 250.0 },
	},
	fragmentShader: [

		'const float PI = 3.14159265359;',
		'const float G = 9.81;',
		'const float KM = 370.0;',
		'const float CM = 0.23;',

		'uniform vec2 u_wind;',
		'uniform float u_resolution;',
		'uniform float u_size;',
		
		'float square (float x) {',
			'return x * x;',
		'}',

		'float omega (float k) {',
			'return sqrt(G * k * (1.0 + square(k / KM)));',
		'}',

		//'float tanh (float x) {',
		//	'return (1.0 - exp(-2.0 * x)) / (1.0 + exp(-2.0 * x));',
		//'}',

		'void main (void) {',
			'vec2 coordinates = gl_FragCoord.xy - 0.5;',
			
			'float n = (coordinates.x < u_resolution * 0.5) ? coordinates.x : coordinates.x - u_resolution;',
			'float m = (coordinates.y < u_resolution * 0.5) ? coordinates.y : coordinates.y - u_resolution;',
			
			'vec2 K = (2.0 * PI * vec2(n, m)) / u_size;',
			'float k = length(K);',
			
			'float l_wind = length(u_wind);',

			'float Omega = 0.84;',
			'float kp = G * square(Omega / l_wind);',

			'float c = omega(k) / k;',
			'float cp = omega(kp) / kp;',

			'float Lpm = exp(-1.25 * square(kp / k));',
			'float gamma = 1.7;',
			'float sigma = 0.08 * (1.0 + 4.0 * pow(Omega, -3.0));',
			'float Gamma = exp(-square(sqrt(k / kp) - 1.0) / 2.0 * square(sigma));',
			'float Jp = pow(gamma, Gamma);',
			'float Fp = Lpm * Jp * exp(-Omega / sqrt(10.0) * (sqrt(k / kp) - 1.0));',
			'float alphap = 0.006 * sqrt(Omega);',
			'float Bl = 0.5 * alphap * cp / c * Fp;',

			'float z0 = 0.000037 * square(l_wind) / G * pow(l_wind / cp, 0.9);',
			'float uStar = 0.41 * l_wind / log(10.0 / z0);',
			'float alpham = 0.01 * ((uStar < CM) ? (1.0 + log(uStar / CM)) : (1.0 + 3.0 * log(uStar / CM)));',
			'float Fm = exp(-0.25 * square(k / KM - 1.0));',
			'float Bh = 0.5 * alpham * CM / c * Fm * Lpm;',

			'float a0 = log(2.0) / 4.0;',
			'float am = 0.13 * uStar / CM;',
			'float Delta = tanh(a0 + 4.0 * pow(c / cp, 2.5) + am * pow(CM / c, 2.5));',

			'float cosPhi = dot(normalize(u_wind), normalize(K));',

			'float S = (1.0 / (2.0 * PI)) * pow(k, -4.0) * (Bl + Bh) * (1.0 + Delta * (2.0 * cosPhi * cosPhi - 1.0));',

			'float dk = 2.0 * PI / u_size;',
			'float h = sqrt(S / 2.0) * dk;',

			'if (K.x == 0.0 && K.y == 0.0) {',
				'h = 0.0;', //no DC term
			'}',
			'gl_FragColor = vec4(h, 0.0, 0.0, 0.0);',
		'}'
	].join('\n')
};
OceanShaders['ocean_phase'] = {
	uniforms: {
		"u_phases": { type: "t", value: null },
		"u_deltaTime": { type: "f", value: null },
		"u_resolution": { type: "f", value: null },
		"u_size": { type: "f", value: null },
	},
	varying: {
		"vUV": { type: "v2" }
	},
	fragmentShader: [

		'const float PI = 3.14159265359;',
		'const float G = 9.81;',
		'const float KM = 370.0;',

		'varying vec2 vUV;',

		'uniform sampler2D u_phases;',
		'uniform float u_deltaTime;',
		'uniform float u_resolution;',
		'uniform float u_size;',

		'float omega (float k) {',
			'return sqrt(G * k * (1.0 + k * k / KM * KM));',
		'}',

		'void main (void) {',
			'vec2 coordinates = gl_FragCoord.xy - 0.5;',
			'float n = (coordinates.x < u_resolution * 0.5) ? coordinates.x : coordinates.x - u_resolution;',
			'float m = (coordinates.y < u_resolution * 0.5) ? coordinates.y : coordinates.y - u_resolution;',
			'vec2 waveVector = (2.0 * PI * vec2(n, m)) / u_size;',

			'float phase = texture2D(u_phases, vUV).r;',
			'float deltaPhase = omega(length(waveVector)) * u_deltaTime;',
			'phase = mod(phase + deltaPhase, 2.0 * PI);',
		
			'gl_FragColor = vec4(phase, 0.0, 0.0, 0.0);',
		'}'
	].join('\n')
};
OceanShaders['ocean_spectrum'] = {
	uniforms: {
		"u_size": { type: "f", value: null },
		"u_resolution": { type: "f", value: null },
		"u_choppiness": { type: "f", value: null },
		"u_phases": { type: "t", value: null },
		"u_initialSpectrum": { type: "t", value: null },
	},
	varying: {
		"vUV": { type: "v2" }
	},
	fragmentShader: [

		'const float PI = 3.14159265359;',
		'const float G = 9.81;',
		'const float KM = 370.0;',

		'varying vec2 vUV;',

		'uniform float u_size;',
		'uniform float u_resolution;',
		'uniform float u_choppiness;',
		'uniform sampler2D u_phases;',
		'uniform sampler2D u_initialSpectrum;',

		'vec2 multiplyComplex (vec2 a, vec2 b) {',
			'return vec2(a[0] * b[0] - a[1] * b[1], a[1] * b[0] + a[0] * b[1]);',
		'}',

		'vec2 multiplyByI (vec2 z) {',
			'return vec2(-z[1], z[0]);',
		'}',

		'float omega (float k) {',
			'return sqrt(G * k * (1.0 + k * k / KM * KM));',
		'}',

		'void main (void) {',
			'vec2 coordinates = gl_FragCoord.xy - 0.5;',
			'float n = (coordinates.x < u_resolution * 0.5) ? coordinates.x : coordinates.x - u_resolution;',
			'float m = (coordinates.y < u_resolution * 0.5) ? coordinates.y : coordinates.y - u_resolution;',
			'vec2 waveVector = (2.0 * PI * vec2(n, m)) / u_size;',

			'float phase = texture2D(u_phases, vUV).r;',
			'vec2 phaseVector = vec2(cos(phase), sin(phase));',

			'vec2 h0 = texture2D(u_initialSpectrum, vUV).rg;',
			'vec2 h0Star = texture2D(u_initialSpectrum, vec2(1.0 - vUV + 1.0 / u_resolution)).rg;',
			'h0Star.y *= -1.0;',

			'vec2 h = multiplyComplex(h0, phaseVector) + multiplyComplex(h0Star, vec2(phaseVector.x, -phaseVector.y));',

			'vec2 hX = -multiplyByI(h * (waveVector.x / length(waveVector))) * u_choppiness;',
			'vec2 hZ = -multiplyByI(h * (waveVector.y / length(waveVector))) * u_choppiness;',

			//no DC term
			'if (waveVector.x == 0.0 && waveVector.y == 0.0) {',
				'h = vec2(0.0);',
				'hX = vec2(0.0);',
				'hZ = vec2(0.0);',
			'}',
		
			'gl_FragColor = vec4(hX + multiplyByI(h), hZ);',
		'}'
	].join('\n')
};
OceanShaders['ocean_normals'] = {
	uniforms: {
		"u_displacementMap": { type: "t", value: null },
		"u_resolution": { type: "f", value: null },
		"u_size": { type: "f", value: null },
	},
	varying: {
		"vUV": { type: "v2" }
	},
	fragmentShader: [

		'varying vec2 vUV;',
		
		'uniform sampler2D u_displacementMap;',
		'uniform float u_resolution;',
		'uniform float u_size;',

		'void main (void) {',
			'float texel = 1.0 / u_resolution;',
			'float texelSize = u_size / u_resolution;',

			'vec3 center = texture2D(u_displacementMap, vUV).rgb;',
			'vec3 right = vec3(texelSize, 0.0, 0.0) + texture2D(u_displacementMap, vUV + vec2(texel, 0.0)).rgb - center;',
			'vec3 left = vec3(-texelSize, 0.0, 0.0) + texture2D(u_displacementMap, vUV + vec2(-texel, 0.0)).rgb - center;',
			'vec3 top = vec3(0.0, 0.0, -texelSize) + texture2D(u_displacementMap, vUV + vec2(0.0, -texel)).rgb - center;',
			'vec3 bottom = vec3(0.0, 0.0, texelSize) + texture2D(u_displacementMap, vUV + vec2(0.0, texel)).rgb - center;',

			'vec3 topRight = cross(right, top);',
			'vec3 topLeft = cross(top, left);',
			'vec3 bottomLeft = cross(left, bottom);',
			'vec3 bottomRight = cross(bottom, right);',
		
			'gl_FragColor = vec4(normalize(topRight + topLeft + bottomLeft + bottomRight), 1.0);',
		'}'
	].join('\n')
};

OceanShaders[ "oceanfft" ] = {

	"u_displacementMap": { type: "t", value: null },
	"u_reflection": { type: "t", value: null },
	"u_normalMap": { type: "t", value: null },
	"u_geometrySize": { type: "f", value: null },
	"u_size": { type: "f", value: null },
	"u_mirrorMatrix": { type: "m4", value: null },
	"u_cameraPosition": { type: "v3", value: null },
	"u_skyColor": { type: "v3", value: null },
	"u_oceanColor": { type: "v3", value: null },
	"u_sunDirection": { type: "v3", value: null },
	"u_exposure": { type: "f", value: null },

},

OceanShaders[ "oceanfft_pars_vertex" ] = [

	'uniform sampler2D u_displacementMap;',
	'uniform float u_geometrySize;',
	'uniform float u_size;',
		
].join('\n');

OceanShaders[ "oceanfft_vertex" ] = [

	'vec3 displacement = texture2D( u_displacementMap, worldPosition.xz * 0.002 ).rgb * ( u_geometrySize / u_size );',
	'vec4 oceanfftWorldPosition = worldPosition + vec4( displacement, 0.0 );',
	
].join('\n');

OceanShaders[ "oceanfft_pars_fragment" ] = [
  
].join('\n');

OceanShaders[ "oceanfft_fragment" ] = [
	
].join('\n');

OceanShaders['ocean_main'] = {
	uniforms: OceanShaders[ "oceanfft" ],

	vertexShader: [
		'precision highp float;',

		'varying vec3 vWorldPosition;',
		'varying vec4 vReflectCoordinates;',

		'uniform mat4 u_mirrorMatrix;',

		OceanShaders[ "screenplane_pars_vertex" ],
		OceanShaders[ "oceanfft_pars_vertex" ],

		'void main (void) {',
		OceanShaders[ "screenplane_vertex" ],

		'vec4 worldPosition = screenPlaneWorldPosition;',

		OceanShaders[ "oceanfft_vertex" ],

		'vWorldPosition = oceanfftWorldPosition.xyz;',
		'vReflectCoordinates = u_mirrorMatrix * oceanfftWorldPosition;',

		'gl_Position = projectionMatrix * viewMatrix * oceanfftWorldPosition;',
		'}'
	].join('\n'),

	vertexShaderNoTexLookup: [
		'precision highp float;',

		'varying vec3 vWorldPosition;',
		'varying vec4 vReflectCoordinates;',

		'uniform mat4 u_mirrorMatrix;',

		OceanShaders[ "screenplane_pars_vertex" ],
		OceanShaders[ "oceanfft_pars_vertex" ],

		'void main (void) {',
		OceanShaders[ "screenplane_vertex" ],

		'vWorldPosition = screenPlaneWorldPosition.xyz;',
		'vReflectCoordinates = u_mirrorMatrix * screenPlaneWorldPosition;',

		'gl_Position = projectionMatrix * viewMatrix * screenPlaneWorldPosition;',
		'}'
	].join('\n'),

	fragmentShader: [
		'varying vec3 vWorldPosition;',
		'varying vec4 vReflectCoordinates;',

		'uniform sampler2D u_reflection;',
		'uniform sampler2D u_normalMap;',
		'uniform vec3 u_oceanColor;',
		"uniform vec3 u_skyColor;",
		'uniform vec3 u_sunDirection;',
		'uniform float u_exposure;',

		'vec3 hdr (vec3 color, float exposure) {',
		'return 1.0 - exp(-color * exposure);',
		'}',

		OceanShaders["screenplane_pars_fragment"],

		'void main (void) {',
		'vec3 normal = texture2D( u_normalMap, vWorldPosition.xz * 0.002 ).rgb;',
		'vec3 view = normalize( vCamPosition - vWorldPosition );',

		// Compute the specular factor
		'vec3 reflection = normalize( reflect( -u_sunDirection, normal ) );',
		'float specularFactor = pow( max( 0.0, dot( view, reflection ) ), 500.0 ) * 20.0;',

		// Get reflection color
		'vec3 distortion = 200.0 * normal * vec3( 1.0, 0.0, 0.1 );',
		'vec3 reflectionColor = texture2DProj( u_reflection, vReflectCoordinates.xyz + distortion ).xyz;',
		'reflectionColor = reflectionColor * u_skyColor;',

		// Smooth the normal following the distance
		'float distanceRatio = min( 1.0, log( 1.0 / length( vCamPosition - vWorldPosition ) * 3000.0 + 1.0 ) );',
		'distanceRatio *= distanceRatio;',
		'distanceRatio = distanceRatio * 0.7 + 0.3;',
		//'distanceRatio = 1.0;',
		'normal = ( distanceRatio * normal + vec3( 0.0, 1.0 - distanceRatio, 0.0 ) ) * 0.5;',
		'normal /= length( normal );',

		// Compute the fresnel ratio
		'float fresnel = pow( 1.0 - dot( normal, view ), 2.0 );',

		// Compute the sky reflection and the water color
		'float skyFactor = ( fresnel + 0.2 ) * 10.0;',
		'vec3 waterColor = ( 1.0 - fresnel ) * u_oceanColor;',

		// Compute the final color
		'vec3 color = ( skyFactor + specularFactor + waterColor ) * reflectionColor + waterColor * 0.5 ;',
		'color = hdr( color, u_exposure );',

		'gl_FragColor = vec4( color, 1.0 );',
		'}'
	].join('\n')
};

export { OceanShaders };
