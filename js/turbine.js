/**
 * Player character
 */
import * as THREE from './3rdParty/three.module.js'

import {GLTFLoader} from './3rdParty/loaders/GLTFLoader.js'
import {DRACOLoader} from './3rdParty/loaders/DRACOLoader.js'

const gltfLoaderDraco = new GLTFLoader();
const dracoLoader = new DRACOLoader();
dracoLoader.setDecoderPath( 'js/3rdParty/loaders/draco/gltf/' );
dracoLoader.setDecoderConfig({type:'js'});
gltfLoaderDraco.setDRACOLoader( dracoLoader );

export const turbine = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                this.clock = new THREE.Clock();
                this.LoadModel();
            }

            update(t) {
                if(this.model) {
                    let delta = this.clock.getDelta();
                    if (this.mixer) this.mixer.update(delta);
                }
            }

            LoadModel() {
                let that        = this;
                let path        = "./models/turbine/";
                let modelFile   = "scene.gltf";
                let file        = path + modelFile;

                gltfLoaderDraco.load(file, function (gltf) {
                    that.model = gltf.scene;
                    that.mixer = new THREE.AnimationMixer( that.model );

                    that.model.traverse( function ( object ) {
                        if (object.isMesh) {
                            object.receiveShadow = true;
                            object.castShadow = true;
                            object.material.side = THREE.FrontSide;
                            //object.material.onBeforeCompile = ModifyShader_;
                        }
                    });

                    that.model.position.set(0, -10, 0);

                    that.model.scale.set(0.02, 0.02, 0.02);
                    that.model.rotation.y = THREE.Math.degToRad(-180);
                    that.model.name = 'turbine';

                    that.loaded = true;
                    that.currentScene.scene.add(that.model);

                    that.animations = gltf.animations;
                    that.currentclip = gltf.animations[2];
                    that.currentAction = that.mixer.clipAction(that.currentclip).play();

                    LOADED++;
                });
            }
        }
    }
})();