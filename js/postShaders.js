/**
 * Setup the scene post processing effects
 */
import * as THREE from './3rdParty/three.module.js'
import {EffectComposer} from './3rdParty/effects/EffectComposer.js'
import {RenderPass} from './3rdParty/effects/RenderPass.js'
import {ShaderPass} from './3rdParty/effects/ShaderPass.js'
import {BokehPass} from './3rdParty/effects/BokehPass.js'
import {UnrealBloomPass} from './3rdParty/effects/UnrealBloomPass.js'
import {FilmPass} from './3rdParty/effects/FilmPass.js'
import {TexturePass} from './3rdParty/effects/TexturePass.js'
import {VignetteShader} from './3rdParty/effects/VignetteShader.js'
import {FXAAShader} from './3rdParty/effects/FXAAShader.js'
import {GUI} from './3rdParty/dat.gui.module.js'
export const postShaders = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            update(t, renderMe) {
                if(POST_PROCESSING && renderMe) {
                    this.render();
                }
            }

            clear(bloom) {
                this.finalComposer.forceClear();

                if(bloom) {
                    this.currentScene.bloomComposer.forceClear();
                }
            }

            // Renders the scene either with or without post-processing shaders
            render() {
                this.currentScene.renderer.setRenderTarget(this.currentScene.renderTarget);
                this.currentScene.renderer.render(this.currentScene.rtScene, this.currentScene.rtCamera);
                this.currentScene.renderer.setRenderTarget(null);

                this.renderBloom(true);
                this.finalComposer.render(0.1);
            }

            // Calls the applicable functions to animate the scene
            animate(e) {
                requestAnimationFrame(this.animateMe);
                this.update(e);
            }

            _Initialize() {
                // Helper variable to keep context within a inner function (try avoid using it)
                let that = this;

                let vertexShader = [
                    "varying vec2 vUv;",
                    "void main() {",
                    "    vUv = uv;",
                    "    gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
                    "}"
                ].join("\n");

                let fragmentShader = [
                    "uniform sampler2D baseTexture;",
                    "uniform sampler2D bloomTexture;",
                    "varying vec2 vUv;",
                    "void main() {",
                    "    gl_FragColor = ( texture2D( baseTexture, vUv ) + vec4( 1.0 ) * texture2D( bloomTexture, vUv ) );",
                    "}"
                ].join("\n");

                var vertShaderNoise = `
                    varying vec2 vUv;
                      void main() {
                        vUv = uv;
                        gl_Position = projectionMatrix 
                          * modelViewMatrix 
                          * vec4( position, 1.0 );
                      }
                `;
                var fragShaderNoise = `
                    uniform float amount;
                      uniform sampler2D tDiffuse;
                      varying vec2 vUv;
                    
                      float random( vec2 p )
                      {
                        vec2 K1 = vec2(
                          23.14069263277926, // e^pi (Gelfond's constant)
                          2.665144142690225 // 2^sqrt(2) (Gelfondâ€“Schneider constant)
                        );
                        return fract( cos( dot(p,K1) ) * 12345.6789 );
                      }
                    
                      void main() {
                        vec4 color = texture2D( tDiffuse, vUv );
                        vec2 uvRandom = vUv;
                        uvRandom.y *= random(vec2(uvRandom.y,amount));
                        float tColor = random(uvRandom)*0.05;
                        color.rgb += tColor;
                        gl_FragColor = vec4(color);
                      }
                `;

                this.currentScene.scene.bloomLayer = new THREE.Layers();
                this.currentScene.scene.bloomLayer.set(BLOOM_SCENE);

                let params;
                if(this.currentScene.effects.bloom.settings) {
                    params = {
                        exposure: this.currentScene.effects.bloom.settings.exposure,
                        bloomStrength: this.currentScene.effects.bloom.settings.bloomStrength,
                        bloomThreshold: this.currentScene.effects.bloom.settings.bloomThreshold,
                        bloomRadius: this.currentScene.effects.bloom.settings.bloomRadius,
                        scene: "Scene with Glow"
                    };
                } else {
                    params = {
                        exposure: 1,
                        bloomStrength: 1.0,
                        bloomThreshold: 0.2,
                        bloomRadius: 0,
                        scene: "Scene with Glow"
                    };
                }
                this.currentScene.darkMaterial = new THREE.MeshBasicMaterial( { color: "black" } );
                //currentScene.materials = {};
                this.currentScene.renderScene = new RenderPass(this.currentScene.scene, this.currentScene.camera);
                this.currentScene.renderScene.clearColor = new THREE.Color( 0, 0, 0 );
                this.currentScene.renderScene.clearAlpha = 0;

                // Depth of field filter
                let bokehParams = {};
                if(this.currentScene.effects.bokeh.settings) {
                    bokehParams = {
                        focus: this.currentScene.effects.bokeh.settings.focus,
                        aperture: this.currentScene.effects.bokeh.settings.aperture,
                        maxblur: this.currentScene.effects.bokeh.settings.maxblur,
                        width: WINDOW_WIDTH,
                        height: WINDOW_HEIGHT
                    }
                } else {
                    bokehParams = {
                        /*focus: 10.0,
                        aperture: 0.05 * 0.00001,
                        maxblur: 0.005,*/
                        /*focus: 120.0,
                        aperture: 3.2 * 0.00001,
                        maxblur: 0.01,*/
                        focus: 10.0,
                        aperture: 4.2 * 0.00001,
                        maxblur: 0.006,

                        width: WINDOW_WIDTH,
                        height: WINDOW_HEIGHT
                    }
                }
                this.bokehPass = new BokehPass(this.currentScene.scene, this.currentScene.camera, bokehParams);
                this.currentScene.bokehPass = this.bokehPass;

                // Bloom filter so that lazers look like lazers :P
                this.bloomPass = new UnrealBloomPass(new THREE.Vector2(WINDOW_WIDTH, WINDOW_HEIGHT), 1.5, 0.4, 0.85);
                this.bloomPass.threshold = params.bloomThreshold;
                this.bloomPass.strength = params.bloomStrength;
                this.bloomPass.radius = params.bloomRadius;
                this.bloomPass.renderToScreen = false;

                this.currentScene.bloomComposer = new EffectComposer(this.currentScene.renderer);
                this.currentScene.bloomComposer.renderToScreen = false;
                this.currentScene.bloomComposer.addPass(this.currentScene.renderScene);
                this.currentScene.bloomComposer.addPass(this.bloomPass);

                this.effectFilm = new FilmPass();
                if(this.currentScene.effects.film.settings) {
                    this.effectFilm.uniforms.grayscale.value = this.currentScene.effects.film.settings.grayscale;
                    this.effectFilm.uniforms.nIntensity.value = this.currentScene.effects.film.settings.nIntensity;
                    this.effectFilm.uniforms.sIntensity.value = this.currentScene.effects.film.settings.sIntensity;
                    this.effectFilm.uniforms.sCount.value = this.currentScene.effects.film.settings.sCount;
                } else {
                    this.effectFilm.uniforms.grayscale.value = false;
                    this.effectFilm.uniforms.nIntensity.value = 0.4;
                    this.effectFilm.uniforms.sIntensity.value = 0.4;
                    this.effectFilm.uniforms.sCount.value = 2048;
                }
                this.effectFilm.renderToScreen = false;

                this.shaderVignette = VignetteShader;
                this.effectVignette = new ShaderPass( this.shaderVignette );
                if(this.currentScene.effects.vignette.settings) {
                    this.effectVignette.uniforms["offset"].value = this.currentScene.effects.vignette.settings.offset;
                    this.effectVignette.uniforms["darkness"].value = this.currentScene.effects.vignette.settings.darkness;
                } else {
                    this.effectVignette.uniforms["offset"].value = 1.3;
                    this.effectVignette.uniforms["darkness"].value = 1.0;
                }
                this.effectVignette.renderToScreen = false;

                const unis = {
                    baseTexture: {value: null}
                }

                // BEGIN adding passes--------------------------------------------------------------------------------------------------------------------------------------------------------------------

                if(this.currentScene.effects.bloom.on) {
                    unis.bloomTexture = {value: this.currentScene.bloomComposer.renderTarget2.texture};
                }

                const finalPass = new ShaderPass(
                    new THREE.ShaderMaterial({
                        uniforms: unis,
                        vertexShader: vertexShader,
                        fragmentShader: fragmentShader,
                        defines: {}
                    }), "baseTexture"
                );
                finalPass.needsSwap = this.currentScene.effects.bloom.on;

                this.finalComposer = new EffectComposer(this.currentScene.renderer);
                this.finalComposer.addPass(this.currentScene.renderScene);
                finalPass.material.transparent = true;
                this.finalComposer.addPass(finalPass);

                if(this.currentScene.effects.vignette.on) {
                    this.finalComposer.addPass(this.effectVignette);
                }

                if(this.currentScene.effects.film.on) {
                    this.finalComposer.addPass(this.effectFilm);
                }

                if(this.currentScene.effects.customNoise.on) {
                    let counter = 0.0;
                    let myEffect = {
                        uniforms: {
                            "tDiffuse": {value: null},
                            "amount": {value: counter}
                        },
                        vertexShader: vertShaderNoise,
                        fragmentShader: fragShaderNoise
                    }
                    let customNoise = new ShaderPass(myEffect);
                    this.finalComposer.addPass(customNoise);
                }

                // Soften the scene
                this.effectFXAA = new ShaderPass( FXAAShader );
                this.effectFXAA.uniforms[ 'resolution' ].value.x = 1 / ( window.innerWidth * WINDOW_PIXEL_RATIO );
                this.effectFXAA.uniforms[ 'resolution' ].value.y = 1 / ( window.innerHeight * WINDOW_PIXEL_RATIO );
                this.finalComposer.addPass( this.effectFXAA );

                if(this.currentScene.effects.bokeh.on) {
                    this.finalComposer.addPass(this.bokehPass);
                }

                //this.gui();

                LOADED++;
            }

            renderBloom(mask) {
                var that = this;
                if (mask === true) {
                    this.currentScene.scene.traverse(function(e) {
                        that.darkenNonBloomed(e, that.currentScene);
                    });
                    this.currentScene.bloomComposer.render();
                    this.currentScene.scene.traverse(this.restoreMaterial);
                } else {
                    this.currentScene.camera.layers.set(BLOOM_SCENE);
                    this.currentScene.bloomComposer.render();
                    this.currentScene.camera.layers.set(ENTIRE_SCENE);
                }
            }

            darkenNonBloomed(obj, thisScene) {
                if (obj.isMesh && thisScene.scene.bloomLayer.test(obj.layers) === false && typeof obj.dontDarken === 'undefined') {
                    materials[obj.uuid] = obj.material;
                    obj.material = this.currentScene.darkMaterial;
                }
            }

            restoreMaterial(obj) {
                if (materials[obj.uuid]) {
                    obj.material = materials[obj.uuid];
                    delete materials[obj.uuid];
                }
            }

            gui() {
                var that = this;

                const effectController = {
                    /*focus: 10.0,
                    aperture: 0.1,
                    maxblur: 0.006*/
                    exposure: 1,
                    strength: 1.0,
                    threshold: 0.2,
                    radius: 0,

                    focus: 10.0,
                    aperture: 4.2,
                    maxblur: 0.006,

                    grayscale: false,
                    nIntensity: .4,
                    sIntensity: .4,
                    sCount: 2048,

                    offset: 1.3,
                    darkness: 1.0
                };

                const matChanger = function () {
                    that.bloomPass.strength = effectController.strength;
                    that.bloomPass.exposure = effectController.exposure;
                    that.bloomPass.threshold = effectController.threshold;
                    that.bloomPass.radius = effectController.radius;

                    that.bokehPass.uniforms["focus"].value = effectController.focus;
                    that.bokehPass.uniforms["aperture"].value = effectController.aperture * 0.00001;
                    that.bokehPass.uniforms["maxblur"].value = effectController.maxblur;

                    that.effectFilm.uniforms["grayscale"].value = effectController.grayscale;
                    that.effectFilm.uniforms["nIntensity"].value = effectController.nIntensity;
                    that.effectFilm.uniforms["sIntensity"].value = effectController.sIntensity;
                    that.effectFilm.uniforms["sCount"].value = effectController.sCount;

                    that.effectVignette.uniforms["offset"].value = effectController.offset;
                    that.effectVignette.uniforms["darkness"].value = effectController.darkness;
                };

                const gui = new GUI();
                let f1 = gui.addFolder('bloom');
                f1.add(effectController, "strength", 0, 5, 0.1).onChange(matChanger);
                f1.add(effectController, "exposure", 0, 5, 0.1).onChange(matChanger);
                f1.add(effectController, "threshold", 0, 5, 0.1).onChange(matChanger);
                f1.add(effectController, "radius", 0, 5, 0.1).onChange(matChanger);

                let f2 = gui.addFolder('bokeh');
                f2.add(effectController, "focus", 0.0, 2000.0, 10).onChange(matChanger);
                f2.add(effectController, "aperture", 0, 20, 0.1).onChange(matChanger);
                f2.add(effectController, "maxblur", 0.0, 0.01, 0.001).onChange(matChanger);

                let f3 = gui.addFolder('film');
                f3.add(effectController, "grayscale", 0, 1, 1).onChange(matChanger);
                f3.add(effectController, "nIntensity", 0, 1, 0.1).onChange(matChanger);
                f3.add(effectController, "sIntensity", 0, 1, 0.1).onChange(matChanger);
                f3.add(effectController, "sCount", 0, 5000, 1).onChange(matChanger);

                let f4 = gui.addFolder('vignette');
                f4.add(effectController, "offset", 0, 3, 0.1).onChange(matChanger);
                f4.add(effectController, "darkness", 0, 3, 0.1).onChange(matChanger);

                gui.close();
                matChanger();
            }
        }
    }
})();