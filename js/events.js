export const events = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                this.events();

                LOADED++;
            }

            onDocumentKeyDown(event) {
                if (event.keyCode === 219) {
                    let newEvent = new Event('playerControlsOn');
                    window.dispatchEvent(newEvent);

                    let newEvent2 = new Event('shipControlsOff');
                    window.dispatchEvent(newEvent2);
                }

                if(event.keyCode === 221) {
                    let newEvent = new Event('shipControlsOn');
                    window.dispatchEvent(newEvent);

                    let newEvent2 = new Event('playerControlsOff');
                    window.dispatchEvent(newEvent2);
                }

                // Pause
                if (event.keyCode === 80) {
                    console.log('pause');
                    if (PawsOn < 1) PawsOn = 1;
                    else PawsOn = 0;
                }
            }

            // Initialise object specific events
            events() {
                var that = this;
                document.addEventListener("keydown", function(event) { that.onDocumentKeyDown(event); }, false);
                window.addEventListener('changeModel', function(e) {
                    if(e.detail.id == 0) {
                        if(that.currentScene.DOFEffect) {
                            that.currentScene.DOFEffect.target = XWING.position;
                            that.currentScene.DOFEffect.circleOfConfusionMaterial.uniforms.focalLength.value = 0.3023;
                        }
                        TROOPERSFOCUS = false;
                        SOLDIERSFOCUS = false;
                        TIEFOCUS = false;
                        XWINGFOCUS = true;
                    } else if(e.detail.id == 1) {
                        if(that.currentScene.DOFEffect) {
                            that.currentScene.DOFEffect.target = TIE.position;
                            that.currentScene.DOFEffect.circleOfConfusionMaterial.uniforms.focalLength.value = 0.6331;
                        }
                        TROOPERSFOCUS = false;
                        SOLDIERSFOCUS = false;
                        XWINGFOCUS = false;
                        TIEFOCUS = true;
                    } else if(e.detail.id == 2) {
                        if(that.currentScene.DOFEffect) {
                            that.currentScene.DOFEffect.target = new THREE.Vector3(375, 76, 225);
                            that.currentScene.DOFEffect.circleOfConfusionMaterial.uniforms.focalLength.value = 0.6331;
                        }
                        TROOPERSFOCUS = true;
                        SOLDIERSFOCUS = false;
                        XWINGFOCUS = false;
                        TIEFOCUS = false;
                    } else if(e.detail.id == 3) {
                        if(that.currentScene.DOFEffect) {
                            that.currentScene.DOFEffect.target = new THREE.Vector3(-375, 47, -225);
                            that.currentScene.DOFEffect.circleOfConfusionMaterial.uniforms.focalLength.value = 0.6331;
                        }
                        TROOPERSFOCUS = false;
                        SOLDIERSFOCUS = true;
                        XWINGFOCUS = false;
                        TIEFOCUS = false;
                    }
                });
            }
        }
    };
})();