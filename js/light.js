/**
 * Setup the lighting for the scene
 */
import * as THREE from './3rdParty/three.module.js';
import {Lensflare, LensflareElement} from './3rdParty/effects/Lensflare.js';
const TextureLoader = new THREE.TextureLoader();
export const light = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {

                if(typeof SKY_COLOUR === 'undefined')
                    SKY_COLOUR = new THREE.Color(1.0, 1.0, 0.5);

                this.playScene = false;

                this.aLight = new THREE.AmbientLight( SKY_COLOUR , 0.2 );
                this.aLight.name = 'ambientLight';
                this.currentScene.scene.add(this.aLight);

                this.events();

                LOADED++;
            }

            events() {
                let that= this;
                window.addEventListener('unlock', function() {
                    that.playScene = true;
                });
                window.addEventListener('keyup', function(e) {
                    if(e.code === 'Space') {
                        that.playScene = !that.playScene;
                    }
                });
            }

            update() {
                if(this.playScene)
                    this.aLight.color = new THREE.Color(SKY_COLOUR.x, SKY_COLOUR.y, SKY_COLOUR.z);
            }
        }
    }
})();