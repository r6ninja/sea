/* General -----------------------------------------------------------------*/
var WINDOW_HEIGHT = window.innerHeight;
var WINDOW_WIDTH = window.innerWidth;
var WINDOW_HEIGHT_MOBILE = WINDOW_HEIGHT;
var WINDOW_WIDTH_MOBILE = WINDOW_WIDTH;
var WINDOW_PIXEL_RATIO = window.devicePixelRatio;
var LOADED          = 0;
var USE_PHYSICS     = false;
var FOG             = false;
var BETTER_FOG      = false;
var BETTER_FOG2     = true;
var POST_PROCESSING = true;
var SELECT_PROCESSING = true;
var FLUID_CAMERA    = true;
var MOBILE          = false;
var MUSIC           = false;
var WINDOW_ACTIVE   = true;
var SIMULATE        = false;

var SHIP           = false;
var SHIPFOCUS      = true;
var SUNSTATUS      = true;
var SUNPROGRESS    = 0.2;
var WAVEPROGRESS    = 0.2;
var SUNPOSITION;
var SKY_COLOUR;
var STATS          = false;
var globalObjects  = [];
var FOCUSONME;

if(WINDOW_HEIGHT > WINDOW_WIDTH) {
    MOBILE = true;
    WINDOW_HEIGHT_MOBILE = WINDOW_HEIGHT/2;
    WINDOW_WIDTH_MOBILE = WINDOW_WIDTH/2;
    WINDOW_PIXEL_RATIO = WINDOW_PIXEL_RATIO/2;
    SELECT_PROCESSING = false;
    $('body').addClass('mobile');
}

if(WINDOW_PIXEL_RATIO > 2) WINDOW_PIXEL_RATIO = 2;

var Intersects;
/* Fog specific vars BEGIN */
var params = {
    //fogNearColor: 0xf9e6d0,
//fogNearColor: 0xbb6d17,
    fogNearColor: 0x383838,
    //fogHorizonColor: 0xf9e6d0,
    //fogHorizonColor: 0x5b442f,
//fogHorizonColor: 0x222423,
    fogHorizonColor: 0xfff5eb,
    //fogHorizonColor: 0x262c2e,
    //fogHorizonColor: 0x3f2f16,
    fogDensity: 0.001,
    fogNoiseSpeed: 200,
    fogNoiseFreq: 0.0024,
    fogNoiseImpact: 0.57
};
var Shaders_ = [];
const ModifyShader_ = (s) => {
    Shaders_.push(s);
    s.uniforms.fogTime = {value: 0.0};

    s.uniforms.fogNearColor = { value: new THREE.Color(params.fogNearColor) };
    s.uniforms.fogNoiseFreq = { value: params.fogNoiseFreq };
    s.uniforms.fogNoiseSpeed = { value: params.fogNoiseSpeed };
    s.uniforms.fogNoiseImpact = { value: params.fogNoiseImpact };
    s.uniforms.time = { value: 0 };
}
/* Fog specific vars END */

// Loaders

/*const Raycaster = new THREE.Raycaster();
var Intersects;
const loadingManager = new THREE.LoadingManager();
const listener = new THREE.AudioListener();
const audioLoader = new THREE.AudioLoader(loadingManager);*/

/* Helper vars for post processing ----------------------------------------------------------*/
var materials = {};
const ENTIRE_SCENE = 0;
const BLOOM_SCENE = 1;

/* Scene vars */
var MinAltMods = [
    'terrain'
];
var GrdZedAlt = 0;
var ControlsLocked = false;
var sceneSize = 1000;

const rtFov = 70;
const rtAspect = sceneSize / sceneSize;
const rtNear = 0.1;
const rtFar = 5;

/* base fragmentShader */
/*

let baseFragment = "uniform float time;\n" +
                "uniform vec2 resolution;\n" +
                "void main()\t{\n" +
                "    gl_FragColor = vec4(0.5, 0.7, 0.2, 1.0);\n" +
                "}";
let baseVertex = "" +
    "varying vec3 vUv; \n" +
    "\n" +
    "    void main() {\n" +
    "      vUv = position; \n" +
    "\n" +
    "      vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);\n" +
    "      gl_Position = projectionMatrix * modelViewPosition; \n" +
    "    }";

*/
var XPOS = [];
var zOffset = 0;
var xOffset = 0;
var pastTime = 0;
var touchPoint;