/**
 * Setup the lighting for the scene
 */
import * as THREE from './3rdParty/three.module.js';
import { Ocean } from './3rdParty/Ocean.js';
import {GUI} from './3rdParty/dat.gui.module.js';
export const sea = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            GUI() {
                const gui = new GUI();
                gui.add( this.ms_Ocean, "size", 100, 5000 ).onChange( function ( v ) {
                    this.object.size = v;
                    this.object.changed = true;
                } );
                gui.add( this.ms_Ocean, "choppiness", 0.1, 4 ).onChange( function ( v ) {
                    this.object.choppiness = v;
                    this.object.changed = true;
                } );
                gui.add( this.ms_Ocean, "windX", - 15, 15 ).onChange( function ( v ) {
                    this.object.windX = v;
                    this.object.changed = true;
                } );
                gui.add( this.ms_Ocean, "windY", - 15, 15 ).onChange( function ( v ) {
                    this.object.windY = v;
                    this.object.changed = true;
                } );
                gui.add( this.ms_Ocean, "sunDirectionX", - 1.0, 1.0 ).onChange( function ( v ) {
                    this.object.sunDirectionX = v;
                    this.object.changed = true;
                } );
                gui.add( this.ms_Ocean, "sunDirectionY", - 1.0, 1.0 ).onChange( function ( v ) {
                    this.object.sunDirectionY = v;
                    this.object.changed = true;
                } );
                gui.add( this.ms_Ocean, "sunDirectionZ", - 1.0, 1.0 ).onChange( function ( v ) {
                    this.object.sunDirectionZ = v;
                    this.object.changed = true;
                } );
                gui.add( this.ms_Ocean, "exposure", 0.0, 0.5 ).onChange( function ( v ) {
                    this.object.exposure = v;
                    this.object.changed = true;
                } );
            }

            _Initialize() {

                this.lastTime = (new Date()).getTime();
                const gsize = 512;
                const res = 1024;
                const gres = res / 2;
                this.ms_Ocean = new Ocean( this.currentScene.renderer, this.currentScene.camera, this.currentScene.scene,
                    {
                        //USE_HALF_FLOAT: false,
                        USE_HALF_FLOAT: true,
                        INITIAL_SIZE: 512.0,
                        INITIAL_WIND: [ 10.0, 10.0 ],
                        INITIAL_CHOPPINESS: 1.5,
                        CLEAR_COLOR: [ 1.0, 1.0, 1.0, 0.0 ],
                        GEOMETRY_ORIGIN: [ 0, 0 ],
                        SUN_DIRECTION: [ - 1.0, 1.0, 1.0 ],
                        OCEAN_COLOR: new THREE.Vector3( 0.004, 0.016, 0.047 ),
                        SKY_COLOR: new THREE.Vector3( 3.2, 9.6, 12.8 ),
                        EXPOSURE: 0.35,
                        GEOMETRY_RESOLUTION: gres,
                        GEOMETRY_SIZE: gsize,
                        RESOLUTION: res,
                        SIZE_OF_FLOAT: 4
                    } );

                this.ms_Ocean.materialOcean.uniforms[ "u_projectionMatrix" ] = { value: this.currentScene.camera.projectionMatrix };
                this.ms_Ocean.materialOcean.uniforms[ "u_viewMatrix" ] = { value: this.currentScene.camera.matrixWorldInverse };
                this.ms_Ocean.materialOcean.uniforms[ "u_cameraPosition" ] = { value: this.currentScene.camera.position };
                this.currentScene.scene.add( this.ms_Ocean.oceanMesh );

                this.GUI();

                LOADED++;
            }

            update() {
                const currentTime = new Date().getTime();
                this.ms_Ocean.deltaTime = (currentTime - this.lastTime) / 1000 || 0.0;
                this.lastTime = currentTime;
                this.ms_Ocean.render(this.ms_Ocean.deltaTime);
                this.ms_Ocean.overrideMaterial = this.ms_Ocean.materialOcean;

                if (this.ms_Ocean.changed) {
                    this.ms_Ocean.materialOcean.uniforms[ "u_size" ].value = this.ms_Ocean.size;
                    this.ms_Ocean.materialOcean.uniforms[ "u_sunDirection" ].value.set( this.ms_Ocean.sunDirectionX, this.ms_Ocean.sunDirectionY, this.ms_Ocean.sunDirectionZ );
                    this.ms_Ocean.materialOcean.uniforms[ "u_exposure" ].value = this.ms_Ocean.exposure;
                    this.ms_Ocean.changed = false;
                }

                this.ms_Ocean.materialOcean.uniforms[ "u_normalMap" ].value = this.ms_Ocean.normalMapFramebuffer.texture;
                this.ms_Ocean.materialOcean.uniforms[ "u_displacementMap" ].value = this.ms_Ocean.displacementMapFramebuffer.texture;
                this.ms_Ocean.materialOcean.uniforms[ "u_projectionMatrix" ].value = this.currentScene.camera.projectionMatrix;
                this.ms_Ocean.materialOcean.uniforms[ "u_viewMatrix" ].value = this.currentScene.camera.matrixWorldInverse;
                this.ms_Ocean.materialOcean.uniforms[ "u_cameraPosition" ].value = this.currentScene.camera.position;
                this.ms_Ocean.materialOcean.depthTest = true;
            }
        }
    }
})();