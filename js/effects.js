/**
 * Setup the lighting for the scene
 */
import * as THREE from './3rdParty/three.module.js';
export const effects = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {

                let that = this;
                this.time = 0;
                this.volume = {x:1};
                this.initiated = false;

                this.listener = new THREE.AudioListener();
                this.actionedPlay = false;
                this.actionedFade = true;
                //this.currentScene.camera.add(listener);

                this.musicTrack = new THREE.Audio(this.listener);
                const audioLoader = new THREE.AudioLoader();
                audioLoader.load( './sounds/thunder.mp3', function( buffer ) {
                    that.musicTrack.setBuffer( buffer );
                    that.musicTrack.setLoop( false );
                    that.musicTrack.setVolume( 2.0 );
                });

                this.events();

                LOADED++;
            }

            update(e) {
            }

            events() {
                let that = this;
                window.addEventListener("thunder", function(event) {
                    if(THUNDER) {
                        that.play();
                    }
                }, false);
            }

            getRandomArbitrary(min, max) {
                return Math.random() * (max - min) + min;
            }

            play() {
                this.musicTrack.play();
                setTimeout(function() {
                    THUNDER = false;
                }, this.getRandomArbitrary(8000, 14000));
            }
        }
    }
})();