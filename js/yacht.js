/**
 * Player character
 */
import * as THREE from './3rdParty/three.module.js'

import {GLTFLoader} from './3rdParty/loaders/GLTFLoader.js'
import {DRACOLoader} from './3rdParty/loaders/DRACOLoader.js'
import {LinearSpline} from './LinearSpline.js';

const gltfLoaderDraco = new GLTFLoader();
const dracoLoader = new DRACOLoader();
dracoLoader.setDecoderPath( 'js/3rdParty/loaders/draco/gltf/' );
dracoLoader.setDecoderConfig({type:'js'});
gltfLoaderDraco.setDRACOLoader( dracoLoader );

export const ship = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            _Initialize() {
                this.clock = new THREE.Clock();
                this.createSplines();
                this.LoadModel();
            }

            update(t) {
                if(this.model) {
                    this.chopiness = (SUNSTATUS) ? this.chopinessSpline.Get(WAVEPROGRESS) : this.chopinessSpline.Get(1 - WAVEPROGRESS);

                    const currentTime = new Date().getTime();

                    this.model.rotation.y = (Math.cos( currentTime * 0.0008 ) * 0.05 - 0.025)*this.chopiness + THREE.Math.degToRad(-225);
                    this.model.rotation.x = (Math.sin( currentTime * 0.001154 + 0.78 ) * 0.1 + 0.05)*this.chopiness;
                }
            }

            createSplines() {
                this.chopinessSpline = new LinearSpline((t, a, b) => {
                    return a + t * (b - a);
                });
                this.chopinessSpline.AddPoint(0.0, 0.75);
                this.chopinessSpline.AddPoint(0.25, 0.01);
                this.chopinessSpline.AddPoint(0.75, 0.75);
                this.chopinessSpline.AddPoint(1.0, 0.01);
            }

            LoadModel() {
                let that        = this;
                let path        = "./models/yacht/";
                let modelFile   = "scene.gltf";
                let file        = path + modelFile;

                gltfLoaderDraco.load(file, function (gltf) {
                    that.model = gltf.scene;
                    that.mixer = new THREE.AnimationMixer( that.model );

                    that.model.traverse( function ( object ) {
                        if (object.isMesh) {
                            object.receiveShadow = true;
                            object.castShadow = true;
                            object.material.side = THREE.FrontSide;
                            //object.material.onBeforeCompile = ModifyShader_;
                        }
                    });

                    that.model.position.set(-148.05, 114, 0);

                    that.model.scale.set(10, 10, 10);
                    that.model.name = 'ship';

                    that.loaded = true;
                    that.currentScene.scene.add(that.model);

                    that.currentScene.DOFEffect.target = that.model.position;
                    that.currentScene.DOFEffect.circleOfConfusionMaterial.uniforms.focalLength.value = 0.6331;

                    //FOCUSONME = that.model.position;

                    LOADED++;
                });
            }
        }
    }
})();