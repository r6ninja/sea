/**
 * Setup the lighting for the scene
 */
import * as THREE from './3rdParty/three.module.js';
import { Ocean } from './3rdParty/FFTOcean.js';
import {GUI} from './3rdParty/dat.gui.module.js';
import {LinearSpline} from './LinearSpline.js';

export const sea = (function() {
    return {
        Main: class {

            constructor(currentScene) {
                this.currentScene = currentScene;
                this._Initialize();
            }

            GUI() {
                let that = this;
                const gui = new GUI();

                gui.add( this.ms_Ocean, "size", 10, 2000 ).listen().onChange( function( v ) {
                    this.object.size = v;
                    this.object.changed = true;
                } );
                gui.add( this.ms_Ocean.materialSpectrum.uniforms.u_choppiness, "value", 0.1, 8 ).listen().name( "choppiness" );
                gui.add( this.ms_Ocean, "windX", -50, 50 ).listen().onChange( function ( v ) {
                    this.object.windX = v;
                    this.object.changed = true;
                } );
                gui.add( this.ms_Ocean, "windY", -50, 50 ).listen().onChange( function ( v ) {
                    this.object.windY = v;
                    this.object.changed = true;
                } );
                gui.add( this.ms_Ocean, "exposure", 0.0, 0.5 ).listen().onChange( function ( v ) {
                    this.object.exposure = v;
                    this.object.changed = true;
                } );
                gui.add( this.ms_Ocean.materialOcean, "wireframe" );
                gui.addColor(this.ms_Ocean, "oceanColor").listen().onChange(function(v) {
                    this.object.oceanColor = v;
                    this.object.changedColor = true;
                });
                gui.addColor(this.ms_Ocean, "skyColor").listen().onChange(function(v) {
                    this.object.skyColor = v;
                    this.object.changedColor = true;
                });
            }

            _Initialize() {

                if(typeof SUNPOSITION === 'undefined') SUNPOSITION = new THREE.Vector3(0.06971397998507745, 0.03489949670250108, -0.9969563611936845);
                this.playScene = false;
                this.showGUI = false;
                this.lastTime = (new Date()).getTime();

                this.ms_MainDirectionalLight = new THREE.DirectionalLight( 0xffffff, 1.5 );
                this.ms_MainDirectionalLight.castShadow = true;
                this.ms_MainDirectionalLight.shadow.mapSize.width = 1024; // default
                this.ms_MainDirectionalLight.shadow.mapSize.height = 1024; // default
                this.ms_MainDirectionalLight.shadow.camera.left = -200;
                this.ms_MainDirectionalLight.shadow.camera.right = 400;
                this.ms_MainDirectionalLight.shadow.camera.top = 200;
                this.ms_MainDirectionalLight.shadow.camera.bottom = -200;
                this.ms_MainDirectionalLight.shadow.camera.near = -200; // default
                this.ms_MainDirectionalLight.shadow.camera.far = 600; // default

                /*this.shadowHelper = new THREE.CameraHelper(this.ms_MainDirectionalLight.shadow.camera);
                this.currentScene.scene.add(this.shadowHelper);*/

                this.ms_MainDirectionalLight.position.set( 0.2, 0.5, -1 );
                this.ms_MainDirectionalLight.name = 'mainDirectionalLight';
                this.currentScene.scene.add( this.ms_MainDirectionalLight );

                this.createSplines();

                const gsize = 512;
                const res = 512;
                const gres = 256;
                this.ms_Ocean = new Ocean( this.currentScene.renderer, this.currentScene.camera, this.currentScene.scene,
                    {
                        USE_HALF_FLOAT: true,
                        //INITIAL_SIZE : 560.0,
                        INITIAL_SIZE : 360.0,
                        INITIAL_WIND : [ 1.0, 10.0 ],
                        //INITIAL_CHOPPINESS : 3.6,
                        INITIAL_CHOPPINESS : 0.2,
                        CLEAR_COLOR : [ 1.0, 1.0, 1.0, 0.0 ],
                        SUN_DIRECTION : SUNPOSITION,
                        //OCEAN_COLOR: new THREE.Vector3( 0.35, 0.4, 0.45 ),
                        OCEAN_COLOR: new THREE.Vector3( 0.00, 0.52, 1.0 ),
                        SKY_COLOR: new THREE.Vector3( 0.49, 0.75, 1.0 ),
                        EXPOSURE : 0.0945,
                        GEOMETRY_RESOLUTION: gres,
                        GEOMETRY_SIZE : gsize,
                        RESOLUTION : res
                    } );

                if(this.showGUI)
                    this.GUI();

                this.events();

                LOADED++;
            }

            createSplines() {
                this.chopinessSpline = new LinearSpline((t, a, b) => {
                    return a + t * (b - a);
                });
                this.chopinessSpline.AddPoint(0.0, 3.4);
                this.chopinessSpline.AddPoint(0.25, 0.2);
                this.chopinessSpline.AddPoint(0.75, 3.4);
                this.chopinessSpline.AddPoint(1.0, 0.2);

                this.windSpline = new LinearSpline((t, a, b) => {
                    return a + t * (b - a);
                });
                this.windSpline.AddPoint(0.0, 50);
                this.windSpline.AddPoint(0.25, 10);
                this.windSpline.AddPoint(0.75, 50);
                this.windSpline.AddPoint(1.0, 10);

                this.skyColourSplineR = new LinearSpline((t, a, b) => {
                    return a + t * (b - a);
                });
                this.skyColourSplineR.AddPoint(0.0, 0.0);
                this.skyColourSplineR.AddPoint(0.17, .905);
                this.skyColourSplineR.AddPoint(0.18, 1.0);
                this.skyColourSplineR.AddPoint(0.19, 0.57);
                this.skyColourSplineR.AddPoint(0.22, 1.0);
                this.skyColourSplineR.AddPoint(1.0, 1.0);

                this.skyColourSplineG = new LinearSpline((t, a, b) => {
                    return a + t * (b - a);
                });
                this.skyColourSplineG.AddPoint(0.0, 0.0);
                this.skyColourSplineG.AddPoint(0.17, 0.454);
                this.skyColourSplineG.AddPoint(0.18, 0.51);
                this.skyColourSplineG.AddPoint(0.19, 0.93);
                this.skyColourSplineG.AddPoint(0.22, 1.0);
                this.skyColourSplineG.AddPoint(1.0, 1.0);

                this.skyColourSplineB = new LinearSpline((t, a, b) => {
                    return a + t * (b - a);
                });
                this.skyColourSplineB.AddPoint(0.0, 0.0);
                this.skyColourSplineB.AddPoint(0.17, 0.517);
                this.skyColourSplineB.AddPoint(0.18, 0.46);
                this.skyColourSplineB.AddPoint(0.19, 1.0);
                this.skyColourSplineB.AddPoint(0.22, 1.0);
                this.skyColourSplineB.AddPoint(1.0, 1.0);
            }

            events() {
                let that= this;
                window.addEventListener('unlock', function() {
                    that.playScene = true;
                });
                window.addEventListener('keyup', function(e) {
                    if(e.code === 'Space') {
                        that.playScene = !that.playScene;
                    }
                });
            }

            update() {
                //if(this.playScene) {
                    const currentTime = new Date().getTime();
                    this.ms_Ocean.deltaTime = (currentTime - this.lastTime) / 1000 || 0.0;
                    this.lastTime = currentTime;

                    if(this.playScene) {
                        if (SUNSTATUS) {
                            this.chopiness = this.chopinessSpline.Get(WAVEPROGRESS);
                            this.windy = this.windSpline.Get(WAVEPROGRESS);
                            this.skyColour = new THREE.Vector3(this.skyColourSplineR.Get(SUNPROGRESS), this.skyColourSplineG.Get(SUNPROGRESS), this.skyColourSplineB.Get(SUNPROGRESS));
                        } else {
                            this.chopiness = this.chopinessSpline.Get(1 - WAVEPROGRESS);
                            this.windy = this.windSpline.Get(1 - WAVEPROGRESS);
                            this.skyColour = new THREE.Vector3(this.skyColourSplineR.Get(1 - SUNPROGRESS), this.skyColourSplineG.Get(1 - SUNPROGRESS), this.skyColourSplineB.Get(1 - SUNPROGRESS));
                        }

                        this.ms_Ocean.sunDirection = SUNPOSITION.clone().multiplyScalar(100);

                        this.ms_MainDirectionalLight.color = new THREE.Color(this.skyColour.x, this.skyColour.y, this.skyColour.z);
                        this.ms_MainDirectionalLight.position.set(this.ms_Ocean.sunDirection.x, this.ms_Ocean.sunDirection.y, this.ms_Ocean.sunDirection.z);

                        this.ms_Ocean.materialSpectrum.uniforms.u_choppiness.value = this.chopiness;
                        this.ms_Ocean.windY = this.windy;
                        this.ms_Ocean.changed = true;
                        this.ms_Ocean.materialOcean.uniforms.u_skyColor.value = this.skyColour;
                        SKY_COLOUR = this.skyColour;
                    }

                    // Render ocean reflection
                    //this.ms_Camera.remove( this.ms_Rain );
                    this.ms_Ocean.render();
                    //if( this.ms_Raining )
                    //this.ms_Camera.add( this.ms_Rain );

                    // Update ocean data
                    this.ms_Ocean.update();
                //}
            }
        }
    }
})();