/**
 * Setup the base prototype scene
 */
import * as THREE from './3rdParty/three.module.js';
import {GUI} from './3rdParty/dat.gui.module.js';
export const scene = (function() {
    return {
        Main: class {

            constructor(farView, containerElement, renderer, effects, fog, betterFog) {
                this.containerElement = containerElement;
                this.renderer = renderer;
                this.farView = farView;
                this.effects = effects;
                this.fog = fog;
                this.fogBetter = betterFog;
                this._Initialize();
            }

            _Initialize() {
                // Helper variable to keep context within a inner function (try avoid using it)
                let that = this;

                this.tempVector = new THREE.Vector3();

                this.container = document.getElementById(this.containerElement);

                // Use more realistic fog
                if(FOG && BETTER_FOG) {
                    this.totalTime_ = 0.0;
                    this.previousRAF_ = 0;
                    this.betterFog();
                } else if(FOG && BETTER_FOG2) {
                    this.betterFog2();
                }

                // Start scene
                this.scene = new THREE.Scene();

                if(FOG || this.fog) {
                    if (!BETTER_FOG && !this.fogBetter && !BETTER_FOG2) {
                        this.scene.fog = new THREE.Fog(0x000000, sceneSize/6, sceneSize/3);
                    } else {
                        //this.scene.fog = new THREE.FogExp2('lightgray', 0.00003);
                        //this.scene.fog = new THREE.FogExp2('lightgray', 0.000005);
                        if(BETTER_FOG) {
                            this.scene.fog = new THREE.FogExp2('lightgray', 0.000005);
                        } else if(BETTER_FOG2) {
                            this.scene.fog = new THREE.FogExp2(params.fogHorizonColor, params.fogDensity);

                            /*var gui = new GUI();
                            gui.add(params, "fogDensity", 0, 0.01, 0.0001).onChange(function() {
                                that.scene.fog.density = params.fogDensity;
                            });
                            gui.addColor(params, "fogHorizonColor").onChange(function() {
                                that.scene.fog.color.set(params.fogHorizonColor);
                                that.scene.background = new THREE.Color(params.fogHorizonColor);
                            });
                            gui.addColor(params, "fogNearColor").onChange(function() {
                                for (let s of Shaders_) {
                                    s.uniforms.fogNearColor = {
                                        value: new THREE.Color(params.fogNearColor)
                                    };
                                }
                            });
                            gui.add(params, "fogNoiseFreq", 0, 0.01, 0.0012).onChange(function() {
                                for (let s of Shaders_) {
                                    s.uniforms.fogNoiseFreq.value = params.fogNoiseFreq;
                                }
                            });
                            gui.add(params, "fogNoiseSpeed", 0, 1000, 100).onChange(function() {
                                for (let s of Shaders_) {
                                    s.uniforms.fogNoiseSpeed.value = params.fogNoiseSpeed;
                                }
                            });
                            gui.add(params, "fogNoiseImpact", 0, 1).onChange(function() {
                                for (let s of Shaders_) {
                                    s.uniforms.fogNoiseImpact.value = params.fogNoiseImpact;
                                }
                            });
                            gui.open();*/
                        }
                    }
                }
                //this.scene.background = new THREE.Color('red');

                //const axesHelper = new THREE.AxesHelper( 200 );
                //this.scene.add( axesHelper );

                // Setup camera for the scene
                this.camera = new THREE.PerspectiveCamera(45, WINDOW_WIDTH / WINDOW_HEIGHT, 0.1, this.farView);
                this.camera.name = 'camera';
                //this.camera.position.set(-210, 105, 300);
                //this.camera.position.set(-174.5228686538279, 123.91017352716885, 213.33181624918487);
                this.camera.position.set(-86.01496649911675, 118.36263369688382, 293.05171885785774);
                this.camPositionVec = new THREE.Vector3(-86.01496649911675, 118.36263369688382, 293.05171885785774);
                FOCUSONME = new THREE.Vector3(40.47999663369536, -4.1456774725532286e-17, -254.41876583556947);
                this.camera.lookAt(0, 0, 0);
                this.camTarget = {x:0, y:0};
                this.mouse = {x:0, y:0};
                this.windowHalf = new THREE.Vector2( WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2 );
                this.scene.add(this.camera);

                this.period = 100;
                this.t = 0.025;
                //this.tScroll = 0.01;
                this.tScroll = 0.06;
                this.camOriginalVector = new THREE.Vector3(this.camera.position.x, this.camera.position.y, this.camera.position.z);
                this.camVector = new THREE.Vector3(this.camera.position.x, this.camera.position.y, this.camera.position.z);
                this.camLookVector = new THREE.Vector3(0, 13, 0);
                this.tempVec = new THREE.Vector3(0, 0, 0);
                this.tempLookVec = new THREE.Vector3(0, 0, 0);
                this.currentPage = [];

                this.rtCamera = new THREE.PerspectiveCamera(rtFov, rtAspect, rtNear, rtFar);
                this.rtScene = new THREE.Scene();
                this.renderTarget = new THREE.WebGLRenderTarget(sceneSize, sceneSize);
                this.rtCamera.position.z = 1;

                // General use clock counter
                this.clock = new THREE.Clock();

                // Start the scene animation
                this.animateMe = function(e) {
                    that.animate(e);
                };

                // Initialise object specific events
                this.events();
            }

            onWindowResize() {
                if(!MOBILE) {
                    WINDOW_HEIGHT = window.innerHeight;
                    WINDOW_WIDTH = window.innerWidth;
                    WINDOW_HEIGHT_MOBILE = WINDOW_HEIGHT;
                    WINDOW_WIDTH_MOBILE = WINDOW_WIDTH;
                    WINDOW_PIXEL_RATIO = window.devicePixelRatio;
                    if (WINDOW_HEIGHT > WINDOW_WIDTH) {
                        WINDOW_HEIGHT_MOBILE = WINDOW_HEIGHT / 2;
                        WINDOW_WIDTH_MOBILE = WINDOW_WIDTH / 2;
                        WINDOW_PIXEL_RATIO = WINDOW_PIXEL_RATIO / 2;
                    }

                    // Update aspect ratio
                    this.camera.aspect = WINDOW_WIDTH / WINDOW_HEIGHT;

                    // Update Projector
                    this.camera.updateProjectionMatrix();

                    // Set the new rendering size
                    this.renderer.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
                    this.renderer.setPixelRatio(WINDOW_PIXEL_RATIO);

                    this.windowHalf = new THREE.Vector2(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
                }
            }

            // Object events
            events() {
                var that = this;
                window.addEventListener('lockControls', function () {
                    ControlsLocked = true;
                }, false);

                window.addEventListener('resize', function () {
                    that.onWindowResize();
                }, false);

                document.addEventListener( 'mousemove', function(event) {
                    that.mouse.x = ( event.clientX - that.windowHalf.x );
                    that.mouse.y = ( event.clientY - that.windowHalf.y );
                }, false );

                document.addEventListener( 'touchmove', function(event) {
                    that.mouse.x = ( event.touches[0].clientX - that.windowHalf.x )*4;
                    that.mouse.y = ( event.touches[0].clientY - that.windowHalf.y )*4;
                }, false );
            }

            fogStep(timeElapsed) {
                this.totalTime_ += timeElapsed;
                for (let s of Shaders_) {
                    s.uniforms.fogTime.value = this.totalTime_;
                }
            }

            update(t) {
                if(!POST_PROCESSING) {
                    this.render();
                }

                let deltaTime = this.clock.getDelta();
                if(FLUID_CAMERA && FOCUSONME) {
                    let x = FOCUSONME.x + -150,
                        z = FOCUSONME.z + 550;

                    this.tempVec.lerp(new THREE.Vector3(x * Math.cos(this.mouse.x * 0.001) + z * Math.sin(this.mouse.x * 0.001), 118.363-(this.mouse.y/10), z * Math.cos(this.mouse.x * 0.001) - x * Math.sin(this.mouse.x * 0.001)), 0.015);

                    this.camera.position.copy(this.tempVec);
                    this.camera.lookAt(new THREE.Vector3(FOCUSONME.x, -4.1456774725532286e-17, FOCUSONME.z));
                    //this.camera.lookAt(new THREE.Vector3(40.47999663369536, -4.1456774725532286e-17, -254.41876583556947));
                }

                if(BETTER_FOG) {
                    if(typeof t === 'undefined')
                        t = 1;

                    //this.fogStep((t - this.previousRAF_) * 0.0001);
                    this.fogStep((t - this.previousRAF_) * 0.0001);
                    if(typeof t !== 'undefined')
                        this.previousRAF_ = t;
                } else if(BETTER_FOG2) {
                    for (let s of Shaders_) {
                        s.uniforms.time.value += deltaTime;
                    }
                }
            }

            // Renders the scene either with or without post-processing shaders
            render() {
                this.renderer.setRenderTarget(this.renderTarget);
                this.renderer.render(this.rtScene, this.rtCamera);
                this.renderer.setRenderTarget(null);

                this.renderer.render(this.scene, this.camera);
            }

            // Calls the applicable functions to animate the scene
            animate(e) {
                requestAnimationFrame(this.animateMe);
                this.update(e);
            }

            betterFog2() {
                const _NOISE_GLSL = `
                        //Classic Perlin 3D Noise 
//by Stefan Gustavson
//
//Source https://gist.github.com/patriciogonzalezvivo/670c22f3966e662d2f83
// 
vec4 permute(vec4 x){return mod(((x*34.0)+1.0)*x, 289.0);}
vec4 taylorInvSqrt(vec4 r){return 1.79284291400159 - 0.85373472095314 * r;}
vec3 fade(vec3 t) {return t*t*t*(t*(t*6.0-15.0)+10.0);}

float cnoise(vec3 P){
  vec3 Pi0 = floor(P); // Integer part for indexing
  vec3 Pi1 = Pi0 + vec3(1.0); // Integer part + 1
  Pi0 = mod(Pi0, 289.0);
  Pi1 = mod(Pi1, 289.0);
  vec3 Pf0 = fract(P); // Fractional part for interpolation
  vec3 Pf1 = Pf0 - vec3(1.0); // Fractional part - 1.0
  vec4 ix = vec4(Pi0.x, Pi1.x, Pi0.x, Pi1.x);
  vec4 iy = vec4(Pi0.yy, Pi1.yy);
  vec4 iz0 = Pi0.zzzz;
  vec4 iz1 = Pi1.zzzz;

  vec4 ixy = permute(permute(ix) + iy);
  vec4 ixy0 = permute(ixy + iz0);
  vec4 ixy1 = permute(ixy + iz1);

  vec4 gx0 = ixy0 / 7.0;
  vec4 gy0 = fract(floor(gx0) / 7.0) - 0.5;
  gx0 = fract(gx0);
  vec4 gz0 = vec4(0.5) - abs(gx0) - abs(gy0);
  vec4 sz0 = step(gz0, vec4(0.0));
  gx0 -= sz0 * (step(0.0, gx0) - 0.5);
  gy0 -= sz0 * (step(0.0, gy0) - 0.5);

  vec4 gx1 = ixy1 / 7.0;
  vec4 gy1 = fract(floor(gx1) / 7.0) - 0.5;
  gx1 = fract(gx1);
  vec4 gz1 = vec4(0.5) - abs(gx1) - abs(gy1);
  vec4 sz1 = step(gz1, vec4(0.0));
  gx1 -= sz1 * (step(0.0, gx1) - 0.5);
  gy1 -= sz1 * (step(0.0, gy1) - 0.5);

  vec3 g000 = vec3(gx0.x,gy0.x,gz0.x);
  vec3 g100 = vec3(gx0.y,gy0.y,gz0.y);
  vec3 g010 = vec3(gx0.z,gy0.z,gz0.z);
  vec3 g110 = vec3(gx0.w,gy0.w,gz0.w);
  vec3 g001 = vec3(gx1.x,gy1.x,gz1.x);
  vec3 g101 = vec3(gx1.y,gy1.y,gz1.y);
  vec3 g011 = vec3(gx1.z,gy1.z,gz1.z);
  vec3 g111 = vec3(gx1.w,gy1.w,gz1.w);

  vec4 norm0 = taylorInvSqrt(vec4(dot(g000, g000), dot(g010, g010), dot(g100, g100), dot(g110, g110)));
  g000 *= norm0.x;
  g010 *= norm0.y;
  g100 *= norm0.z;
  g110 *= norm0.w;
  vec4 norm1 = taylorInvSqrt(vec4(dot(g001, g001), dot(g011, g011), dot(g101, g101), dot(g111, g111)));
  g001 *= norm1.x;
  g011 *= norm1.y;
  g101 *= norm1.z;
  g111 *= norm1.w;

  float n000 = dot(g000, Pf0);
  float n100 = dot(g100, vec3(Pf1.x, Pf0.yz));
  float n010 = dot(g010, vec3(Pf0.x, Pf1.y, Pf0.z));
  float n110 = dot(g110, vec3(Pf1.xy, Pf0.z));
  float n001 = dot(g001, vec3(Pf0.xy, Pf1.z));
  float n101 = dot(g101, vec3(Pf1.x, Pf0.y, Pf1.z));
  float n011 = dot(g011, vec3(Pf0.x, Pf1.yz));
  float n111 = dot(g111, Pf1);

  vec3 fade_xyz = fade(Pf0);
  vec4 n_z = mix(vec4(n000, n100, n010, n110), vec4(n001, n101, n011, n111), fade_xyz.z);
  vec2 n_yz = mix(n_z.xy, n_z.zw, fade_xyz.y);
  float n_xyz = mix(n_yz.x, n_yz.y, fade_xyz.x); 
  return 2.2 * n_xyz;
}
                        `;

                THREE.ShaderChunk.fog_fragment = `
                    #ifdef USE_FOG
  vec3 windDir = vec3(0.0, 0.0, time);
  vec3 scrollingPos = vFogWorldPosition.xyz + fogNoiseSpeed * windDir;  
  float noise = cnoise(fogNoiseFreq * scrollingPos.xyz);
  float vFogDepth = (1.0 - fogNoiseImpact * noise) * fogDepth;
  #ifdef FOG_EXP2
  float fogFactor = 1.0 - exp( - fogDensity * fogDensity * vFogDepth * vFogDepth );
  #else
  float fogFactor = smoothstep( fogNear, fogFar, vFogDepth );
  #endif
  gl_FragColor.rgb = mix( gl_FragColor.rgb, mix(fogNearColor, fogColor, fogFactor), fogFactor );
#endif
`;

                THREE.ShaderChunk.fog_pars_fragment = `
                    #ifdef USE_FOG
  ${_NOISE_GLSL}
uniform vec3 fogColor;
  uniform vec3 fogNearColor;
varying float fogDepth;
#ifdef FOG_EXP2
uniform float fogDensity;
#else
uniform float fogNear;
uniform float fogFar;
#endif
  varying vec3 vFogWorldPosition;
  uniform float time;
  uniform float fogNoiseSpeed;
  uniform float fogNoiseFreq;
  uniform float fogNoiseImpact;
#endif
`;

                THREE.ShaderChunk.fog_vertex = `
                    #ifdef USE_FOG
  fogDepth = - mvPosition.z;
   vFogWorldPosition = (modelMatrix * vec4( transformed, 1.0 )).xyz;
#endif
`;

                THREE.ShaderChunk.fog_pars_vertex = `
                    #ifdef USE_FOG
  varying float fogDepth;
  varying vec3 vFogWorldPosition;
#endif
`;
            }

            betterFog() {
                const _NOISE_GLSL = `
                        //
                        // Description : Array and textureless GLSL 2D/3D/4D simplex
                        //               noise functions.
                        //      Author : Ian McEwan, Ashima Arts.
                        //  Maintainer : stegu
                        //     Lastmod : 20201014 (stegu)
                        //     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
                        //               Distributed under the MIT License. See LICENSE file.
                        //               https://github.com/ashima/webgl-noise
                        //               https://github.com/stegu/webgl-noise
                        //
                        vec3 mod289(vec3 x) {
                          return x - floor(x * (1.0 / 289.0)) * 289.0;
                        }
                        vec4 mod289(vec4 x) {
                          return x - floor(x * (1.0 / 289.0)) * 289.0;
                        }
                        vec4 permute(vec4 x) {
                             return mod289(((x*34.0)+1.0)*x);
                        }
                        vec4 taylorInvSqrt(vec4 r)
                        {
                          return 1.79284291400159 - 0.85373472095314 * r;
                        }
                        float snoise(vec3 v)
                        {
                          const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
                          const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);
                        // First corner
                          vec3 i  = floor(v + dot(v, C.yyy) );
                          vec3 x0 =   v - i + dot(i, C.xxx) ;
                        // Other corners
                          vec3 g = step(x0.yzx, x0.xyz);
                          vec3 l = 1.0 - g;
                          vec3 i1 = min( g.xyz, l.zxy );
                          vec3 i2 = max( g.xyz, l.zxy );
                          //   x0 = x0 - 0.0 + 0.0 * C.xxx;
                          //   x1 = x0 - i1  + 1.0 * C.xxx;
                          //   x2 = x0 - i2  + 2.0 * C.xxx;
                          //   x3 = x0 - 1.0 + 3.0 * C.xxx;
                          vec3 x1 = x0 - i1 + C.xxx;
                          vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y
                          vec3 x3 = x0 - D.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y
                        // Permutations
                          i = mod289(i);
                          vec4 p = permute( permute( permute(
                                     i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
                                   + i.y + vec4(0.0, i1.y, i2.y, 1.0 ))
                                   + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));
                        // Gradients: 7x7 points over a square, mapped onto an octahedron.
                        // The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)
                          float n_ = 0.142857142857; // 1.0/7.0
                          vec3  ns = n_ * D.wyz - D.xzx;
                          vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)
                          vec4 x_ = floor(j * ns.z);
                          vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)
                          vec4 x = x_ *ns.x + ns.yyyy;
                          vec4 y = y_ *ns.x + ns.yyyy;
                          vec4 h = 1.0 - abs(x) - abs(y);
                          vec4 b0 = vec4( x.xy, y.xy );
                          vec4 b1 = vec4( x.zw, y.zw );
                          //vec4 s0 = vec4(lessThan(b0,0.0))*2.0 - 1.0;
                          //vec4 s1 = vec4(lessThan(b1,0.0))*2.0 - 1.0;
                          vec4 s0 = floor(b0)*2.0 + 1.0;
                          vec4 s1 = floor(b1)*2.0 + 1.0;
                          vec4 sh = -step(h, vec4(0.0));
                          vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
                          vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;
                          vec3 p0 = vec3(a0.xy,h.x);
                          vec3 p1 = vec3(a0.zw,h.y);
                          vec3 p2 = vec3(a1.xy,h.z);
                          vec3 p3 = vec3(a1.zw,h.w);
                        //Normalise gradients
                          vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
                          p0 *= norm.x;
                          p1 *= norm.y;
                          p2 *= norm.z;
                          p3 *= norm.w;
                        // Mix final noise value
                          vec4 m = max(0.5 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
                          m = m * m;
                          return 105.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1),
                                                        dot(p2,x2), dot(p3,x3) ) );
                        }
                        float FBM(vec3 p) {
                          float value = 0.0;
                          float amplitude = 0.5;
                          float frequency = 0.0;
                          for (int i = 0; i < 6; ++i) {
                            value += amplitude * snoise(p);
                            p *= 2.0;
                            amplitude *= 0.5;
                          }
                          return value;
                        }
                        `;

                THREE.ShaderChunk.fog_fragment = `
                    #ifdef USE_FOG
                      vec3 fogOrigin = cameraPosition;
                      vec3 fogDirection = normalize(vWorldPosition - fogOrigin);
                      float fogDepth = distance(vWorldPosition, fogOrigin);
                      // f(p) = fbm( p + fbm( p ) )
                      vec3 noiseSampleCoord = vWorldPosition * 0.00025 + vec3(
                          0.0, 0.0, fogTime * 0.025);
                      float noiseSample = FBM(noiseSampleCoord + FBM(noiseSampleCoord)) * 0.5 + 0.5;
                      fogDepth *= mix(noiseSample, 1.0, saturate((fogDepth - 5000.0) / 5000.0));
                      fogDepth *= fogDepth;
                      float heightFactor = 0.55;
                      //float heightFactor = 1.0;
                      float fogFactor = heightFactor * exp(-fogOrigin.y * fogDensity) * (
                          1.0 - exp(-fogDepth * fogDirection.y * fogDensity)) / fogDirection.y;
                      fogFactor = saturate(fogFactor);
                      gl_FragColor.rgb = mix( gl_FragColor.rgb, fogColor, fogFactor );
                    #endif`;

                THREE.ShaderChunk.fog_pars_fragment = _NOISE_GLSL + `
                    #ifdef USE_FOG
                      uniform float fogTime;
                      uniform vec3 fogColor;
                      varying vec3 vWorldPosition;
                      #ifdef FOG_EXP2
                        uniform float fogDensity;
                      #else
                        uniform float fogNear;
                        uniform float fogFar;
                      #endif
                    #endif`;

                THREE.ShaderChunk.fog_vertex = `
                    #ifdef USE_FOG
                      vWorldPosition = worldPosition.xyz;
                    #endif`;

                THREE.ShaderChunk.fog_pars_vertex = `
                    #ifdef USE_FOG
                      varying vec3 vWorldPosition;
                    #endif`;
            }
        }
    }
})();